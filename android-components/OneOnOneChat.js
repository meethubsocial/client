/*
Written by Svitlana Galianova (UI part) & Giorgi Osadze (fetch part)
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import imgStyle from '../styles/img';
import formStyle from '../styles/form';
import buttonStyle from '../styles/button';
import Menu from '../elements/Menu';
import Message from '../elements/Message';
import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity, FlatList
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import { List, ListItem } from "react-native-elements";

export default class OneOnOneChat extends Component<{}> {
  constructor(props) {
	  super(props);
    this.state = { friendUsername: this.props.navigation.state.params.username,
      messages:[
        /*{id:"1", username:"Me", msg:"Hi"},
        {id:"3", username:this.props.navigation.state.params.username, msg:"Hey, are you ready for the midterm?"},
        {id:"4", username:"Me", msg:"Do we have a PRJ midterm as well????"},
        {id:"5", username:this.props.navigation.state.params.username, msg:"Yes... He said that a month ago"},
        {id:"6", username:"Me", msg:"A month??? Why didn't I know about that?"},
        {id:"7", username:this.props.navigation.state.params.username, msg:"You weren't in a class that day"},
        {id:"8", username:this.props.navigation.state.params.username, msg:"But I texted you on Discord"},
        {id:"9", username:"Me", msg:"Oh... I must have missed it"},
        {id:"10", username:this.props.navigation.state.params.username, msg:"It's not big deal, just finish your parts of a project and you are good. Also did you finish working on chat already? I need it for testing"},
        {id:"11", username:"Me", msg:"Not yet, I am working on it right now"},
        {id:"12", username:this.props.navigation.state.params.username, msg:"Ok, let me know when you are done"}*/
      ], isGroupChat: true, myUsername:"", newMsg:""
    };
    //get my username
    fetch('http://myvmlab.senecacollege.ca:6094/viewProfile', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        session: this.props.navigation.state.params.session,
      }),
    }).then((response) => response.json())
    .then((responseData) => {
      if(responseData.success != false){
        this.setState({
          myUsername: responseData.query[0].Username,
        });
      }else{
        alert("Something went wrong");
      }
    }).catch(function(error) {
      alert('An unexpected error occurred. Please try again later.');
      throw error;
    });

    //"socket" get messages
    this.getMsg = setInterval(()=>{
      this.setState({messages:[]}, ()=> {
        fetch('http://myvmlab.senecacollege.ca:6094/getMessages', {
            method: 'POST',
            headers: { Accept: 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify({
              session:this.props.navigation.state.params.session,
              username: this.state.friendUsername
            })
          }).then((response) => response.json())
          .then((responseData) => {
            if (responseData.success != false){
              var newArray = this.state.messages;
              for (i = 0; i < responseData.result.length; i++) {
                var m = {
                  id: responseData.result[i].MsgId,
                  username: responseData.result[i].Username,
                  msg: responseData.result[i].Content
                }
                newArray.push(m);
              }
              this.setState({ messages: newArray });
            }else{
              alert("Something went wrong");
            }
          }).catch(function (error) {
            alert('An unexpected error occurred. Please try again later.');
            throw error;
          });
        }); 
      },1000);
    
		//to access session: {this.props.navigation.state.params.session}
    }
    componentWillUnmount(){
      clearInterval(this.getMsg)
  }
    onHandleSend(){
      if(this.state.newMsg == ""){
        alert("You can't send a blank message");
      }else{
        fetch('http://myvmlab.senecacollege.ca:6094/sendMessage', {
            method: 'POST',
            headers: { Accept: 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify({
              //friend's username
              username:this.state.friendUsername,
              message:this.state.newMsg,
              session:this.props.navigation.state.params.session,
              sender:this.state.myUsername
            })
          }).then((response) => response.json())
          .then((responseData) => {
            if (responseData.success != false){
              this.setState({newMsg:""});
            }else{
              alert("Something went wrong");
            }
          }).catch(function (error) {
            alert('An unexpected error occurred. Please try again later.');
            throw error;
          }); 
      }
    }
    render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		    <View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>
            <View style={containerStyle.hubTitle}>
              <Text style={textStyle.hubTitle}>{this.state.friendUsername}</Text>
            </View>
            <View style={[containerStyle.container,{marginBottom:15}]}>
              <FlatList
                style={containerStyle.chat}
                behavior="padding" keyboardShouldPersistTaps="handled"
                data={this.state.messages}
                renderItem={({ item }) => (
                  <Message id={item.id} username={item.username} msg={item.msg}
                  isMe={(this.state.myUsername==item.username)?true:false}
                  />
                )}
                keyExtractor={item => item.id}
              />
        </View>
        <View style={containerStyle.container}>
          <TextInput
              underlineColorAndroid={'transparent'}
              style={formStyle.msgInputField}
              onChangeText={(text) => this.setState({ newMsg: text })}
              value={this.state.newMsg}
              placeholder={"Type your message here..."}
              placeholderTextColor="#379683"
              onSubmitEditing={this.onHandleSend.bind(this)}/>
        </View>
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
