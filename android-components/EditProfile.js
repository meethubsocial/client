/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import formStyle from '../styles/form';
import buttonStyle from '../styles/button';
import imgStyle from '../styles/img';
import Menu from '../elements/Menu';
import Panel from '../elements/Panel';
import customValidation from '../elements/validationWrapper';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, Keyboard, BackAndroid
} from 'react-native';

import { StackNavigator } from 'react-navigation';

export default class EditProfile extends Component<{}> {
  constructor(props) {
	  super(props);
		this.state = {
			//edit profile 
			bio: '',
			imgUri: 'https://www.publicdomainpictures.net/pictures/80000/velka/woman-profile-silhouette-clipart.jpg',
			imgUriPlaceholder:'http://i67.tinypic.com/2zf4mrb.jpg',
			firstName:'', lastName:'',dob:'',username:'',nameError:'',dobError:'',usernameError:'',
			bioError:'',imgError:'',errored:0,imgWarning:'',
			//Change password
			oldPass:'', newPass:'', confirmNewPass:'',
			oldPassError:'', newPassError:'', confirmError:'',
		}; 
		//get initial data for edit profile:
		fetch('http://myvmlab.senecacollege.ca:6094/viewProfile', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				session: this.props.navigation.state.params.session,
			}),
		}).then((response) => response.json())
		.then((responseData) => {
			if(responseData.success != false){
				this.setState({
					firstName: responseData.query[0].First_Name,
					lastName: responseData.query[0].Last_Name,
					dob: responseData.query[0].DateOfBirth,
					username: responseData.query[0].Username,
					imgUri: responseData.query[0].Picture,
					bio: responseData.query[0].Bio,
				});
			}else{
				alert("Something went wrong");
			}
		}).catch(function(error) {
			alert('An unexpected error occurred. Please try again later.');
			throw error;
		});
		//to access session: {this.props.navigation.state.params.session}
	}
	onHandleLogout(){
		fetch('http://myvmlab.senecacollege.ca:6094/logout', {
			method: 'POST',
			headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				session: this.props.navigation.state.params.session
			}),
		}).then((response) => response.json())
		.then((responseData) => {
			if(responseData.status != ''){
				BackAndroid.exitApp();
			}else{
				alert("Something went wrong. Please try again later.");
			}
		}).catch(function(error) {
			alert('An unexpected error occurred. Please try again later.');
			// ADD THIS THROW error
			throw error;
		});
	}
	onHandlePasswordChange(){
		var eCount = 0;

		const oError = customValidation.validate('isEmpty', this.state.oldPass);
		(oError == undefined)? eCount : eCount++;
		const npError = customValidation.validate('password', this.state.newPass);
		(npError == undefined)? eCount : eCount++;
		const cnpError = customValidation.isValidConfirmPassword(this.state.newPass, this.state.confirmNewPass);
		(cnpError == "")? eCount : eCount++;
		
		this.setState({
			oldPassError: oError, newPassError: npError, confirmError: cnpError,
		},()=>{
			if(eCount<=0){
				//alert("Submit: Change Password");
				fetch('http://myvmlab.senecacollege.ca:6094/updatePassword', {
					method: 'POST',
					headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						session: this.props.navigation.state.params.session,
						oldPass: this.state.oldPass,
						newPass: this.state.newPass
					}),
				}).then((response) => response.json())
				.then((responseData) => {
					if(responseData.success != false){
						alert("Your password was updated successfuly");
					}else{
						this.setState({oldPassError: responseData.error});
					}
				}).catch(function(error) {
					alert('An unexpected error occurred. Please try again later.');
					// ADD THIS THROW error
					throw error;
				});
			}
		});
	}
	onHandleCancel(){
		alert("Edit Profile: Cancel");
	}
	onHandleUpdate(){
		Keyboard.dismiss();
		//trimming 
		this.setState({
			bio:this.state.bio.trim(),imgUri:this.state.imgUri.trim(),firstName:this.state.firstName.trim(),
			lastName:this.state.lastName.trim(),dob:this.state.dob.trim(),username:this.state.username.trim(),
		},()=>{
			//other validation
			var eCount = 0;
			const uError = customValidation.validate('username', this.state.username);
			(uError == undefined)? eCount : eCount++;
			var nError="";
			const fnError = customValidation.isValidName(this.state.firstName);
			(fnError == "")? nError : nError=fnError;
			const lnError = customValidation.isValidName(this.state.lastName);
			(lnError == "")? nError : nError=lnError;
			(nError == "")? eCount : eCount++;
			const dError =  customValidation.isValidDate(this.state.dob);
			(dError == "")? eCount : eCount++;
			const iuError = customValidation.isValidImgUri(this.state.imgUri);
			(iuError == "")? eCount : eCount++;
			const bError = customValidation.isValidBio(this.state.bio);
			(bError == "")? eCount : eCount++;
			this.setState({ 
				usernameError: uError, nameError:nError,dobError:dError,imgError:iuError,errored:eCount,
				bioError:bError,
			},()=>{
				if(this.state.errored<=0){
					//valid data
					//alert("Edit Profile: Update");
					fetch('http://myvmlab.senecacollege.ca:6094/updateUser', {
						method: 'POST',
						headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							session: this.props.navigation.state.params.session,
							username: this.state.username,
							fname: this.state.firstName,
							lname: this.state.lastName,
							bio: this.state.bio,
							birth: this.state.dob,
							picture: (this.state.imgUri=="")?this.state.imgUriPlaceholder:this.state.imgUri,
						}),
					}).then((response) => response.json())
					.then((responseData) => {
						if(responseData.success != "false"){
							//redirect to View my Profile
							var session = this.props.navigation.state.params.session;
							this.props.navigation.navigate('MyProfile', {session});
						}else{
							alert("Something went wrong. Please try again later.");
						}
					}).catch(function(error) {
						alert('An unexpected error occurred. Please try again later.');
						// ADD THIS THROW error
						throw error;
					});
				}
			});
		});
	}
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		<View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}> 
				{/*<Text style={[textStyle.fullName, {marginBottom:10}]}>Edit My Profile</Text> */}
				<Panel title="Edit My Profile">
					<Text style={textStyle.error}>{this.state.nameError}</Text>
					<View style={containerStyle.inlineContainer}>
						<TextInput
							underlineColorAndroid={'transparent'}
							style={formStyle.inlineInputField}
							value={this.state.firstName}
							onChangeText={(text) => this.setState({ firstName: text })}
							placeholder={"First Name"} 
							placeholderTextColor="#379683" />
						<TextInput
							underlineColorAndroid={'transparent'}
							style={[formStyle.inlineInputField,{marginLeft:20}]}
							value={this.state.lastName}
							onChangeText={(text) => this.setState({ lastName: text })}
							placeholder={"Last Name"} 
							placeholderTextColor="#379683" />
					</View>
					<View style={containerStyle.inlineContainer}>
						<View style={containerStyle.mainComponent}>
							<Text style={textStyle.error}>{this.state.usernameError}</Text>
							<TextInput
								underlineColorAndroid={'transparent'}
								style={formStyle.inlineInputField}
								value={this.state.username}
								onChangeText={(text) => this.setState({ username: text })}
								placeholder={"Username"} 
								placeholderTextColor="#379683" />
							<Text style={textStyle.error}>{this.state.dobError}</Text>
							<TextInput
								underlineColorAndroid={'transparent'}
								style={formStyle.inlineInputField}
								value={this.state.dob}
								onChangeText={(text) => this.setState({ dob: text })}
								placeholder={"DD/MM/YYYY"} 
								placeholderTextColor="#379683" />
							<Text style={textStyle.error}>{this.state.imgError}</Text>
							<TextInput
								underlineColorAndroid={'transparent'}
								style={formStyle.inlineInputField}
								value={this.state.imgUri}
								onChangeText={(text) => this.setState({ imgUri: text })}
								placeholder={("Image Link (.jpg)")} 
								placeholderTextColor="#379683" />
						</View>
						<View style={{justifyContent: 'flex-start', alignItems: 'flex-start',}}>
							<Text style={textStyle.warning}>{this.state.imgWarning}</Text>
							<Image ref={'img'} style={[imgStyle.editImg]} source={{
								uri: (this.state.imgUri==''||this.state.imgError!='') ? this.state.imgUriPlaceholder : this.state.imgUri}}
								onError={(error)=>{
									this.refs['img'].setNativeProps({src: [{uri: this.state.imgUriPlaceholder}]})
									this.setState({
										imgWarning: "Image link is invalid",
									});
								}}/>
						</View>
					</View>
					<Text style={textStyle.error}>{this.state.bioError}</Text>
					<TextInput
						multiline={true}
						maxLength = {255}
						underlineColorAndroid={'transparent'}
						style={formStyle.textArea}
						value={this.state.bio}
						onChangeText={(text) => this.setState({ bio: text })}
						placeholder={"Tell us a bit about yourself!"} 
						placeholderTextColor="#379683" />
					<View style={containerStyle.inlineContainer}>
						<Text style={buttonStyle.nonActiveButton} onPress={this.onHandleCancel.bind(this)}>Cancel</Text>
						<Text style={buttonStyle.activeButton} onPress={this.onHandleUpdate.bind(this)}>Update</Text>
					</View>
				</Panel>
				<Panel title="Change My Password">
					<Text style={textStyle.error}>{this.state.oldPassError}</Text>
					<TextInput
		       	  		secureTextEntry={true}
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						onChangeText={(text) => this.setState({ oldPass: text })} 
						placeholder={"Old Password"} 
						placeholderTextColor="#379683"
						value={this.state.oldPass}/>
					<Text style={textStyle.error}>{this.state.newPassError}</Text>
					<TextInput
						secureTextEntry={true}
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						onChangeText={(text) => this.setState({ newPass: text })} 
						placeholder={"New Password"} 
						placeholderTextColor="#379683"
						value={this.state.newPass}/>
					<Text style={textStyle.error}>{this.state.confirmError}</Text>
					<TextInput
						secureTextEntry={true}
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						onChangeText={(text) => this.setState({ confirmNewPass: text })} 
						placeholder={"Confirm New Password"} 
						placeholderTextColor="#379683"
						value={this.state.confirmNewPass}/>
					<Text style={buttonStyle.activeButton} onPress={this.onHandlePasswordChange.bind(this)}>Submit</Text>
				</Panel>
				<Panel title="Logout">
					<Text style={buttonStyle.activeButton} onPress={this.onHandleLogout.bind(this)}>Logout</Text>
				</Panel>
	    	</View>
		</View>
	  </ScrollView>
	  );
  }
}
