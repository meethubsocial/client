/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, FlatList
} from 'react-native';

import { StackNavigator } from 'react-navigation';
import { List, ListItem } from "react-native-elements";

export default class Messages extends Component<{}> {
  constructor(props) {
		super(props);
		this.onHandleChatRedirect = this.onHandleChatRedirect.bind(this);
		this.state = { 
			messages:[
				//UI test data
			/*{id:"1", username:"Giorgi", msg:"See you in class", pic:"https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg"},
			{id:"2", username:"Eric", msg:"Hello, How are you doing?", pic:"http://i67.tinypic.com/2zf4mrb.jpg"},
			{id:"3", username:"Ronen", msg:"Hey, are you ready for the midterm?", pic:"https://cdn.pixabay.com/photo/2016/09/11/01/06/dandelion-1660518_1280.jpg"},
			{id:"4", username:"John", msg:"Do we have a midterm today????", pic:"https://upload.wikimedia.org/wikipedia/commons/9/9e/Psephotus_dissimilis_%28female%29_-Burgers_Zoo-8a-3c.jpg"},
			{id:"5", username:"Lily", msg:"He said that a month ago", pic:"https://www.publicdomainpictures.net/pictures/80000/velka/woman-profile-silhouette-clipart.jpg"},
			{id:"6", username:"Rachel", msg:"Why didn't I know about that?", pic:"https://upload.wikimedia.org/wikipedia/commons/thumb/1/13/Clyde_The_Bulldog.jpg/768px-Clyde_The_Bulldog.jpg"},
			{id:"7", username:"Ross", msg:"You weren't in a class that day",pic:"https://upload.wikimedia.org/wikipedia/commons/5/59/Turdus_merula_-Manchester%2C_England_-male-8-3c.jpg"},
			{id:"8", username:"Andrew", msg:"But I texted you on Discord",pic:"https://cdn.pixabay.com/photo/2015/10/31/11/58/binoculars-1015267_1280.jpg"},
			{id:"9", username:"Maria", msg:"Oh... I must have missed it",pic:"https://cdn.pixabay.com/photo/2018/03/23/14/27/time-3253837_1280.jpg"},
			{id:"10", username:"Cris", msg:"Also did you finish working on lab 1?",pic:"https://upload.wikimedia.org/wikipedia/commons/d/d2/Coreopsis_floridana_%28Florida_tickseed%29_%288240005185%29.jpg"},
			{id:"11", username:"Bob", msg:"Not yet, maybe tomorrow?", pic:"https://cdn.pixabay.com/photo/2015/10/30/10/40/key-1013662_1280.jpg"},
			{id:"12", username:"Ann", msg:"Ok, I have to go",pic:"https://cdn.pixabay.com/photo/2017/10/03/20/25/girls-2814009_1280.jpg"}
			*/]
	}; 
		//to access session: {this.props.navigation.state.params.session}
		fetch('http://myvmlab.senecacollege.ca:6094/getAllMessages', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				session: this.props.navigation.state.params.session,
			}),
		}).then((response) => response.json())
		.then((responseData) => {
			if(responseData.success != false){
				for (i = 0; i < responseData.result.length; i++) {
					var msg = {
						id: i,
						username: responseData.result[i].Username,
						msg: responseData.result[i].Content,
						pic: responseData.result[i].Picture
					}
					var newArray = this.state.messages.slice();
					newArray.push(msg);
					this.setState({ messages: newArray });
				}
			}else{
				alert("Something went wrong");
			}
		}).catch(function(error) {
			alert('An unexpected error occurred. Please try again later.');
			throw error;
		});
    }
  onHandleChatRedirect(item){
		//alert(item.username);
		var session = this.props.navigation.state.params.session;
		var username = item.username;
    this.props.navigation.navigate('OneOnOneChat',{session, username});
	}  
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
			<View style={containerStyle.container}>
				<Menu {...this.props}/>
	      <View style={containerStyle.mainComponent}>  
						<Text style={textStyle.fullName}>My Messages</Text>

						<View style={[containerStyle.container,{marginBottom:15}]}>
            {/*List of messages from this.state*/}  
              <FlatList
                style={containerStyle.msgList}
                behavior="padding" keyboardShouldPersistTaps="handled"
                data={this.state.messages}
                renderItem={({ item }) => (
                  <ListItem
									  key={item.id}
									  onPress={() => this.onHandleChatRedirect(item)}
										roundAvatar
										title={item.username}
										subtitle={item.msg}
										avatar={item.pic}
										containerStyle={{ borderBottomWidth: 0 }}
										hideChevron={true}
									/>
                )}
                keyExtractor={item => item.id}
              />
        </View>

		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
