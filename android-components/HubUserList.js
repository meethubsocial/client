/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import buttonStyle from '../styles/button';
import imgStyle from '../styles/img';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity, Alert
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class HubUserList extends Component<{}> {
  constructor(props) {
	  super(props);
	  this.addFriend = this.addFriend.bind(this);
	  this.sendMsg = this.sendMsg.bind(this);
	  this.viewProfile = this.viewProfile.bind(this);
		this.state = { 
			users: [
				/*{id: 1, name:'Rachel Green', username:'Bob123', pic:'http://pixel.nymag.com/imgs/daily/vulture/2014/12/17/17-rachel-green-jewish.w529.h352.jpg'},
				{id: 2, name:'Monica Geller', username:'m0n1ca.geller', pic:'http://filmesegames.com.br/wp-content/uploads/2014/09/Monica-Geller-monica-geller-25963717-1035-1280.jpg'},
				{id: 3, name:'Ross Geller', username:'r0ss.geller', pic:'https://img.buzzfeed.com/buzzfeed-static/static/2014-06/18/10/campaign_images/webdr07/33-of-the-most-memorable-ross-geller-moments-on-f-2-27100-1403100262-2_dblbig.jpg'},
				{id: 4, name:'Phoebe Buffay', username:'ph0ebe.buff@y', pic:'https://media1.popsugar-assets.com/files/thumbor/g1jJskizyWCzTH17cXNj2BGx0Ns/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2017/07/25/859/n/1922398/98c795d259779df2c77e61.43422979_edit_img_cover_file_43785815_1500923262/i/Phoebe-Buffay-Quotes-From-Friends.jpg'},
				{id: 5, name:'Joey Tribbiani', username:'j0ey.tribbi@ni', pic:'https://kinoafisha.ua/upload/2014/02/news/n6b/15/40466/b_1393571705mett-leblan-rasskazal-o-svoei-licsnoi-jizni.jpg'},
				{id: 6, name:'Chandler Bing', username:'ch@ndler.b1ng', pic:'http://www.freepngimg.com/download/lion/3-2-lion-png.png'}*/
			], hubName: this.props.navigation.state.params.hubName, hubId: this.props.navigation.state.params.hubId
		}; 
		//to access session: {this.props.navigation.state.params.session}		
		fetch('http://myvmlab.senecacollege.ca:6094/userInHub', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				session: this.props.navigation.state.params.session,
				hubId: this.state.hubId
			}),
		}).then((response) => response.json())
		.then((responseData) => {
			if (responseData.success != false) {
				//insert
				if (responseData.result.length > 0) {
					for (j=0; j < responseData.result.length; j++) {
						for (i = 0; i < responseData.result[j].length; i++) {
							var friend = {
								id: responseData.result[j][i].UserId,
								name: responseData.result[j][i].First_Name + " " + responseData.result[j][i].Last_Name,
								username: responseData.result[j][i].Username,
								pic: responseData.result[j][i].Picture,
							}
							var newArray = this.state.users.slice();
							newArray.push(friend);
							this.setState({ users: newArray });
						}
					}
				}
			} else {
				alert("Something went wrong");
			}
		}).catch(function(error) {
			alert('An unexpected error occurred. Please try again later.'+error);
			throw error;
		});
	}
	sendMsg(user){
		//redirect to dialog page
		var session = this.props.navigation.state.params.session;
		var username = user.username;
    	this.props.navigation.navigate('OneOnOneChat',{session, username});
	} 
	shareLocation(){
		Alert.alert(
			"Share My Location",
			"I agree to share my current location with all my friends",
			[{text: 'Yes', onPress: () => {
					alert("Your location has been successfully shared with your friends");	
					//fetch request to share location
				}},
				{text: 'Cancel', onPress: () => {}, style: 'cancel'},],
			{ cancelable: false }
		);
	}
  	addFriend(user){
		Alert.alert(
			"Let's be friends!",
			"Send "+user.name+" friends request?",
			[{text: 'Yes', onPress: () => {
				alert("Sent");	
				//fetch request to add friend
				fetch('http://myvmlab.senecacollege.ca:6094/addFriend', {
					method: 'POST',
					headers: {
						Accept: 'application/json',
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({
						session: this.props.navigation.state.params.session,
						username: user.username,
						stat:1
					}),
				}).then((response) => response.json())
					.then((responseData) => {
						if (responseData.success != false) {
							//update list of friends
							alert(user.name+" was added to your friend list");
							var session = this.props.navigation.state.params.session;
							var hubId = this.state.hubId
							var hubName = this.state.hubName
							this.props.navigation.navigate('HubUserList',{session, hubId, hubName});
						} else {
							alert("Something went wrong");
						}
					}).catch(function (error) {
						alert('An unexpected error occurred. Please try again later.');
						throw error;
					}); 
				}},
				{text: 'Cancel', onPress: () => {}, style: 'cancel'},
			],
			{ cancelable: false }
		);
	}  
	viewProfile(user){
		var session = this.props.navigation.state.params.session;
		var username = user.username;
    	this.props.navigation.navigate('FriendProfile',{session, username});
	}
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
			<View style={containerStyle.container}>
				<Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>  
            <View style={containerStyle.hubTitle}>
                <Text style={textStyle.hubTitle}>{this.state.hubName}</Text>
            </View>
				{this.state.users.length > 0 ?
					this.state.users.map(( user ) => {
		    		return (
						<View key={user.id} style={[containerStyle.nameContainer, {marginTop: 10,  justifyContent: 'center', alignItems: 'center'}]}>
								
							<View style={[containerStyle.inlineContainer, {margin:3}]}>
								<Image style={imgStyle.profilePic} source={{uri: user.pic}}/>

								<View style={containerStyle.columnContainer}>
									<View style={[containerStyle.container, {backgroundColor:'#8EE4AF', margin:5}]}>
										<Text style={textStyle.smallDarkBlue} onPress={()=>this.viewProfile(user)}>{user.name}</Text>
									</View>
									<View style={[containerStyle.container, {backgroundColor:'#8EE4AF', margin:5}]}>
										<Text style={textStyle.smallDarkBlue} onPress={()=>this.viewProfile(user)}>{user.username}</Text>
									</View>

									<View style={[containerStyle.inlineContainer,{justifyContent: 'center', alignItems: 'center'}]}>
										<TouchableOpacity onPress={()=>this.sendMsg(user)}>
											<Image style={imgStyle.menuIcon} source={require('../img/message.png')}/>
										</TouchableOpacity>
										<TouchableOpacity onPress={()=>this.shareLocation(user)}>
											<Image style={imgStyle.menuIcon} source={require('../img/hub.png')} />
										</TouchableOpacity>

										<TouchableOpacity onPress={()=>this.addFriend(user)}>
											<Image style={imgStyle.menuIcon} source={require('../img/addFriend.png')} />
										</TouchableOpacity>
									</View>
								</View>
							</View>
						</View>
					)}):<Text>You are the only member of this hub for now</Text>
				}
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
