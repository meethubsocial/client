import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import imgStyle from '../styles/img';
import Menu from '../elements/Menu';
import mapStyle from '../styles/maps';
import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity
} from 'react-native';

import MapView  from 'react-native-maps';
import {Marker} from 'react-native-maps';
import {Circle} from 'react-native-maps';

import { StackNavigator } from 'react-navigation';


export default class InHub extends Component<{}> {
  constructor(props) {
	  super(props);

    // set initial state of region
    var longdelta = 0.05
    var latdelta = 0.05
		this.state = { hubs: [], hubName: 'MyHub', hubId: this.props.navigation.state.params.userHubId,
                   friends: [{latitude:10,longitude:10,username:''}],
                   region:{
                     latitude: 0,
                     longitude: 0,
                     latitudeDelta: latdelta,
                     longitudeDelta: longdelta
                   }
                 };


    var session = this.props.navigation.state.params.session;
    //alert("current session is "+session)


    fetch('http://myvmlab.senecacollege.ca:6094/allhubs', {
        method: 'POST',
        headers: { Accept: 'application/json', 'Content-Type': 'application/json'}
    },).then(res => res.json()).then( (fetch_hubs) => {
          this.setState({hubs : fetch_hubs});
        }).catch((error) => {
          throw error;
          console.error("Could not get all hubs, server could be disconnected");
        });

    var intervalId = setInterval( ()=>{
        fetch('http://myvmlab.senecacollege.ca:6094/viewFriends', {
          method: 'POST',
          headers: {
                  	Accept: 'application/json',
                  	'Content-Type': 'application/json',
                  	},
                  	body: JSON.stringify({
                        session:session,}),
        }).then((response) => response.json())
        .then((responseData) => {
          //alert(JSON.stringify(responseData.fullList))
          var fullList = responseData.result;
          this.setState({friends:fullList.fullList})
          //alert(JSON.stringify(fullList.fullList[0]));
        }).catch((error)=>{console.log("error with viewFriends")});
    },5000);
    //alert("current hub "+this.props.navigation.state.params.userHubId)
		//to access session: {this.props.navigation.state.params.session}
    //this.setState({ region: {latitude:0, longitude:0, latitudeDelta:0, longitudeDelta 0} });
    // Get the loction of the hubName
    // get the range of the hubName
    navigator.geolocation.getCurrentPosition( ( position ) => {

  	    var lat = parseFloat(position.coords.latitude);
  	    var long = parseFloat(position.coords.longitude);
        var latdelta = 0.01;
        var longdelta = 0.01;
  	    var initialRegion = {
      		latitude: lat,
      		longitude: long,
      		latitudeDelta: latdelta,
      		longitudeDelta: longdelta
  	    };
        // set the region of the user
  	    this.setState({ region: initialRegion });

  	},(error) => this.setState({ error: error.message }),)
    }
    viewHubChat(){
        var session = this.props.navigation.state.params.session
        var hubId = this.state.hubId
        var hubName = this.state.hubName
        this.props.navigation.navigate('HubChat',{session, hubId, hubName});
    }
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		    <View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>
                <View style={containerStyle.hubTitle}>
                    <Text style={textStyle.hubTitle}>{this.state.hubName}</Text>
                    <TouchableOpacity onPress={this.viewHubChat.bind(this)}>
                        <Image style={imgStyle.topMenuIcon} source={require('../img/message.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={mapStyle.inHub}>
                    <MapView
                      style={mapStyle.map}
                      region={this.state.region}
                      onRegionChange={this.onRegionChange}>

                      {

                        // draw all friends on the map
                        this.state.friends.map(( friend ) => {
                              return (
                                <MapView.Marker
                                  key={JSON.stringify(friend)} // key needed for every element on the map
                                  // center
                                  pinColor="#CCFFA0" // color of the pin
                                  coordinate={{longitude:friend.longitude , latitude:friend.latitude}}
                                  //radius={(hub.Diameter/2)}
                                  title={friend.Username}>
                                </MapView.Marker>
                              )
                         })

                      }

                      { /* Draw all Circles */

                        this.state.hubs.map(( hub ) => { // map over every hub and place a circle in its location
                          if (typeof hub.Longitude == 'undefined' || typeof hub.Latitude == 'undefined'){
                              //alert(JSON.stringify(hub))
                              return;
                          }
                          return (
                              <MapView.Circle
                              key={JSON.stringify(hub)}
                              // center
                              center={{longitude:hub.Longitude , latitude:hub.Latitude}}
                              radius={(hub.Diameter/2)}
                              title="You">
                              </MapView.Circle>
                          )
                         })
                        /* Draw all of the hubs to the screen */
                      }
                      <MapView.Marker coordinate={{ longitude:this.state.region.longitude,
                                  latitude:this.state.region.latitude }}
                                            title="You">
                      </MapView.Marker>
                    </MapView>

                    { /* Draw all Circles */

                      this.state.hubs.map(( hub ) => { // map over every hub and place a circle in its location
                        if (typeof hub.Longitude == 'undefined' || typeof hub.Latitude == 'undefined'){
                            //alert(JSON.stringify(hub))
                            return;
                        } else {}
                        return (
                            <MapView.Circle
                            key={JSON.stringify(hub)}
                            // center
                            center={{longitude:hub.Longitude , latitude:hub.Latitude}}
                            radius={(hub.Diameter/2)}
                          title="You">
                            </MapView.Circle>
                        )
                       })
                      /* Draw all of the hubs to the screen */
                    }

                    {/*
                        to redirect to a different page do this:
                        var session = this.props.navigation.state.params.session;
    		                this.props.navigation.navigate('NameOfThePage',{session});
                    */}
                </View>
        </View>
	    </View>
	  </ScrollView>
	  );
  }
}
