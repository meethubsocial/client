/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import imgStyle from '../styles/img';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity, Alert
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class MyProfile extends Component<{}> {
  constructor(props) {
	  super(props);
		this.state = { 
			fullName: 'Rachel Green', lastActive: '4 hours ago', dob: '1/11/1900', username:'r@chel.green',
			imgUri: 'http://www.uriux.com/wp-content/uploads/2017/09/female-placeholder.jpg',
			bio: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi maximus sagittis laoreet. Cras venenatis velit sit amet enim laoreet, ac bibendum enim molestie. Nunc vulputate diam aucto',
			imgUriPlaceholder: 'http://i67.tinypic.com/2zf4mrb.jpg',
	  }; 
	//to access session: {this.props.navigation.state.params.session}
	//send state to Node.js server side
	fetch('http://myvmlab.senecacollege.ca:6094/viewFriendProfile', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			session: this.props.navigation.state.params.session,
			username: this.props.navigation.state.params.username
		}),
	}).then((response) => response.json())
	.then((responseData) => {
		if(responseData.success != false){
			this.setState({
				fullName: responseData.query[0].First_Name+" "+responseData.query[0].Last_Name,
				dob: responseData.query[0].DateOfBirth,
				username: responseData.query[0].Username,
				imgUri: responseData.query[0].Picture,
				bio: responseData.query[0].Bio,
			});
		}else{
			alert("Something went wrong");
		}
	}).catch(function(error) {
		alert('An unexpected error occurred. Please try again later.');
		throw error;
	});
  }
  viewDialog(){
	var session = this.props.navigation.state.params.session;
	var username = this.state.username;
	this.props.navigation.navigate('OneOnOneChat',{session, username});
	}
	removeFriend(){
		Alert.alert(
			"Remove a friend",
			"Are you sure, you want to end friendship with "+this.state.fullName+"?",
			[{text: 'Yes', onPress: () => {
					alert("Remove");	
					//fetch request to remove friend
					//fetch request to remove friend
					fetch('http://myvmlab.senecacollege.ca:6094/deleteFriend', {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							session: this.props.navigation.state.params.session,
							username: this.state.username
						}),
					}).then((response) => response.json())
						.then((responseData) => {
							if (responseData.success != false) {
								//update list of friends
								alert(this.state.fullName+" was removed from your friend list");
								//go back to the friend list
								var session = this.props.navigation.state.params.session;
								this.props.navigation.navigate('FriendsList', {session});
							} else {
								alert("Something went wrong");
							}
						}).catch(function (error) {
							alert('An unexpected error occurred. Please try again later.');
							throw error;
						}); 
				}},
				{text: 'Cancel', onPress: () => {}, style: 'cancel'},
			],
			{ cancelable: false }
	)}
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled" >
		<View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>

		      <View style={containerStyle.nameContainer}>
					<View style={containerStyle.inlineContainer}>
						<View style={containerStyle.container}>
							<Text style={[textStyle.fullName, {marginTop: 20}]}>{this.state.fullName}</Text>
							<View style={[containerStyle.inlineContainer, {margin: 5, justifyContent: 'center', alignItems: 'center'}]}>
							<TouchableOpacity onPress={this.viewDialog.bind(this)}>
								<Image style={[imgStyle.menuIcon, {marginTop: 5}]} source={require('../img/message.png')}/>
							</TouchableOpacity>
							<TouchableOpacity onPress={this.removeFriend.bind(this)}>
								<Image style={imgStyle.menuIcon} source={require('../img/x.png')} />
							</TouchableOpacity>
							{/*<View style={[containerStyle.container, {backgroundColor: '#8EE4AF', marginLeft: 10}]}>
								<Text style={textStyle.smallDarkBlue}>Last active:</Text>
								<Text style={textStyle.smallDarkBlue}>{this.state.lastActive}</Text>
	  						</View>*/}
						</View>
					</View>
					<Image 
						style={[imgStyle.profilePic, {marginRight:10}]} 
						source={{uri: this.state.imgUri}} 
						onError={(error)=>{this.refs['img'].setNativeProps({src: [{uri: this.state.imgUriPlaceholder}]})
					}}/>
				</View>
			</View>

			<View style={[containerStyle.nameContainer, {marginTop: 10}]}>
				<View style={containerStyle.inlineContainer}>
					<View style={[containerStyle.container, {backgroundColor: '#8EE4AF', margin: 20}]}>
						<Text style={textStyle.smallDarkBlue}>{this.state.dob}</Text>
					</View>
					<View style={[containerStyle.container, {backgroundColor: '#8EE4AF', margin: 20}]}>
						<Text style={textStyle.smallDarkBlue}>{this.state.username}</Text>
					</View>
					</View>
					<View style={[containerStyle.container, {backgroundColor: '#8EE4AF', margin: 30}]}>
						<Text style={textStyle.smallDarkBlue}>{this.state.bio}</Text>
					</View>
				</View>

		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
