import containerStyle from '../styles/container';
import mapStyle from '../styles/maps';
import Menu from '../elements/Menu';
import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, Alert
} from 'react-native';
import MapView  from 'react-native-maps';
import {Marker} from 'react-native-maps';
import {Circle} from 'react-native-maps';

import { StackNavigator } from 'react-navigation';

export default class HubsMap extends Component<{}> {
    constructor(props) {
    	super(props);

    	var longdelta = 0.05
    	var latdelta = 0.05

    	this.state  = {
    	    region:{
    		latitude: 0,
    		longitude: 0,
    		latitudeDelta: latdelta,
    		longitudeDelta: longdelta
    	    },
    	    hubs:[]
    	};
      fetch('http://myvmlab.senecacollege.ca:6094/updateloc', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
        latitude : this.state.region.latitude,
        longitude: this.state.region.longitude,
        session: this.props.navigation.state.params.session
        }),
      }).then((response) => response.json())
      .then((responseData) => {

      }).catch(function(error) {
          alert('An unexpected error occurred. Please try again later.');
          // ADD THIS THROW error
          throw error;
      });

      // get every hub from server
      fetch('http://myvmlab.senecacollege.ca:6094/allhubs', {
        method: 'POST',
        headers: { Accept: 'application/json', 'Content-Type': 'application/json'}
      },).then(res => res.json()).then( (fetch_hubs) => {
          this.setState({hubs : fetch_hubs});
        }).catch((error) => {
          throw error;
          console.error("Could not get all hubs, server could be disconnected");
        });

      // get geolocation
      navigator.geolocation.getCurrentPosition( ( position ) => {

      var lat = parseFloat(position.coords.latitude);
      var long = parseFloat(position.coords.longitude);

      var initialRegion = {
        latitude: lat,
        longitude: long,
        latitudeDelta: latdelta,
        longitudeDelta: longdelta
      };
      this.setState({ region: initialRegion });

      },(error) => this.setState({ error: error.message }),) // get geolocation

      var intervalId = setInterval( ()=>{
            // update user locations
            fetch('http://myvmlab.senecacollege.ca:6094/updateloc', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
              latitude : this.state.region.latitude,
              longitude: this.state.region.longitude,
              session: this.props.navigation.state.params.session
              }),
            }).then((response) => response.json())
            .then((responseData) => {

            }).catch(function(error) {
                alert('An unexpected error occurred. Please try again later.');
                // ADD THIS THROW error
                throw error;
            });

        // get all locations again
        fetch('http://myvmlab.senecacollege.ca:6094/allhubs', {
        	    method: 'POST',
        	    headers: { Accept: 'application/json', 'Content-Type': 'application/json'}
        	},).then(res => res.json()).then( (fetch_hubs) => {
                this.setState({hubs : fetch_hubs});
              }).catch((error) => {
                throw error;
                console.error("Could not get all hubs, server could be disconnected");
              });


      	// get geolocation
        navigator.geolocation.getCurrentPosition( ( position ) => {

      	    var lat = parseFloat(position.coords.latitude);
      	    var long = parseFloat(position.coords.longitude);

      	    var initialRegion = {
          		latitude: lat,
          		longitude: long,
          		latitudeDelta: latdelta,
          		longitudeDelta: longdelta
      	    };
      	    this.setState({ region: initialRegion });



      	 },(error) => this.setState({ error: error.message }),) // get geolocation
      	   //to access session: {this.props.navigation.state.params.session}
         },5000); // interval

  } // constructor

    onRegionChange(region) {}

    ComponentDidMount() {

      fetch('http://myvmlab.senecacollege.ca:6094/allhubs', {
    	    method: 'POST',
    	    headers: { Accept: 'application/json', 'Content-Type': 'application/json'}
    	},).then(res => res.json()).then( (fetch_hubs) => {
            this.setState({hubs : fetch_hubs});
      }).catch((error) => {
            throw error;
            console.error("Could not get all hubs, server could be disconnected");
      });

    }
    componentWillUnmount() {
      clearInterval(this.state.intervalId);
      console.log('Did Unmount');

    }
    test(){ alert('test') }
    render() {
	return (
	<ScrollView style={containerStyle.init}
	  behavior="padding" keyboardShouldPersistTaps="handled">
	  <Menu {...this.props} />
	  <View style={[containerStyle.mainComponent, {height: 600}]}>
	    <MapView style={mapStyle.map}
	             region={this.state.region}
	             onRegionChange={this.onRegionChange}>

	        { /* Draw all Circles */

      		  this.state.hubs.map(( hub ) => { // map over every hub and place a circle in its location
        			if (typeof hub.Longitude == 'undefined' || typeof hub.Latitude == 'undefined'){
        			    //alert(JSON.stringify(hub))
        			    return;
        			}
        		  return (
            			<MapView.Circle
            			key={JSON.stringify(hub)}
            			// center
            			center={{longitude:hub.Longitude , latitude:hub.Latitude}}
            			radius={(hub.Diameter/2)}
            			title="You">
            			</MapView.Circle>
        		  )
      		   })
		        /* Draw all of the hubs to the screen */
		      }
	        { // Draw all markers

        		 this.state.hubs.map(( hub ) => { // map over every hub and place a marker
        		     if (typeof hub.Longitude == 'undefined' || typeof hub.Latitude == 'undefined'){ return; }
        		    return (
        			    <MapView.Marker
                    key={JSON.stringify(hub.HubId)} // key needed for every element on the map
              			// center
              			pinColor="#0CFFA0" // color of the pin
              			coordinate={{longitude:hub.Longitude , latitude:hub.Latitude}}
                    onPress={() => {
                      // check if user is in hub
                      fetch('http://myvmlab.senecacollege.ca:6094/isinhub', {
                          method: 'POST',
                          headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                          },
                          body: JSON.stringify({
                          latitude : this.state.region.latitude,
                          longitude: this.state.region.longitude,
                          session: this.props.navigation.state.params.session,
                          hubId:hub.HubId
                          }),
                        }).then((response) => response.json())
                          .then((responseData) => {
                          if(responseData.res=="touching" && responseData.colHub == hub.HubId) {
                            // navigate to hub screen
                            Alert.alert(
                              'Hub Trigger',
                              'To enter hub ID '+hub.HubId+' click okay',
                              [
                                {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {text: 'OK', onPress: () => {
                                  console.log('OK Pressed')
                                  fetch('http://myvmlab.senecacollege.ca:6094/appendUser', {
                                      method: 'POST',
                                      headers: {
                                        Accept: 'application/json',
                                        'Content-Type': 'application/json',
                                      },
                                      body: JSON.stringify({
                                      latitude : this.state.region.latitude,
                                      longitude: this.state.region.longitude,
                                      session: this.props.navigation.state.params.session,
                                      userHub:hub.HubId
                                    })})
                                  var session = this.props.navigation.state.params.session;
                                  this.props.navigation.navigate('InHub', {session,userHubId:hub.HubId});
                                }},
                              ],
                              { cancelable: false }
                            ) // alert dialog

                          }
                          else {
                            //
                          }
                        }).catch(function(error) {
                            alert('An unexpected error occurred. Please try again later.');
                            // ADD THIS THROW error
                            throw error;
                        });
                      }

                    }
              			title={hub.HubName}>

                  </MapView.Marker>
        		    )
        		})
            // Draw all markers
		      }
    		<MapView.Marker coordinate={{ longitude:this.state.region.longitude,
    					      latitude:this.state.region.latitude }}
    	                        title="You">
    		</MapView.Marker>
	    </MapView>
	  </View>
	</ScrollView>

	);
  }
}
