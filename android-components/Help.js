import containerStyle from '../styles/container';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, 
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class Help extends Component<{}> {
  constructor(props) {
	  super(props);
		this.state = { username: ''}; 
		//to access session: {this.props.navigation.state.params.session}
    }
    
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		<View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>  
		        {/*HELP GOES HERE*/}
			<Text>HELP</Text>
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
