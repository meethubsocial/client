import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import formStyle from '../styles/form';
import buttonStyle from '../styles/button';
import Menu from '../elements/Menu'
import mapStyle from '../styles/maps';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, CheckBox, Keyboard,
} from 'react-native';
import MapView  from 'react-native-maps';
import {Marker} from 'react-native-maps';
import {Circle} from 'react-native-maps';

import { StackNavigator } from 'react-navigation';


export default class CreateHub extends Component<{}> {
  constructor(props) {
    super(props);

    this.state = {longitude: 0, latitude: 0};

    navigator.geolocation.getCurrentPosition( ( position ) => {

        var lat_ = parseFloat(position.coords.latitude);
        var long_ = parseFloat(position.coords.longitude);
        var latdelta = 0.04;
        var longdelta = 0.04;

        var initialRegion = {
            latitude: lat_,
            longitude: long_,
            latitudeDelta: latdelta,
            longitudeDelta: longdelta
        };
        this.setState({ region: initialRegion });

    },(error) => {
        this.setState({ error: error.message })
      }) // get geolocation

    //to access newHub: {this.props.navigation.state.params.newHub}


  }
  componentWillUnmount() {

  }
  componentDidMount(){

  }
  onHandleCancel(){

    var session = this.props.navigation.state.params.session;
    this.props.navigation.navigate('ListHubs',{session});
    //alert("Cancel");
  }

  onHandleCreate(){
      //      this.setState({latitude: this.state.region.latitude);
      //    this.setState({longitude: this.state.region.longitude);
      //    this.props.navigation.state.params.this.state.region.latitude

      //      this.setState({latitude: this.state.region.latitude);
      //    this.setState({longitude: this.state.region.longitude);

      //this.state = { newHub:{ hubName: '', range: '',checked:false,startTime:'', endTime:'',}};
      var h = this.props.navigation.state.params.newHub;
      h.longitude = this.state.longitude;
      h.latitude = this.state.latitude;















      fetch('http://myvmlab.senecacollege.ca:6094/isinhub', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            latitude : h.latitude,
            longitude: h.longitude,
            session: this.props.navigation.state.params.session,
            range:h.range,
            hubId:0
          }),
        }).then((response) => response.json())
          .then((responseData) => {
          if(responseData.res=="touching") {
            // navigate to hub screen
            alert("cannot create hub due to overlap with another hub")
          }
          else {
            fetch('http://myvmlab.senecacollege.ca:6094/makeHub', {
                method: 'POST',
                headers: { Accept: 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify({
                  longitude: h.longitude,
                  latitude: h.latitude,
                  hubName:h.hubName,
                  diameter:h.range,
                  session:this.props.navigation.state.params.session
                })
              }).then(res => res.json()).then( () => { })
              .catch((error) => {
                //console.error(error);
                alert("error");
                throw error;
              });
          }
        }).catch(function(error) {
            alert('An unexpected error occurred. Please try again later.');
            // ADD THIS THROW error
            throw error;
        });





















        var session = this.props.navigation.state.params.session;
        this.props.navigation.navigate('HubsMap',{session});
  }

  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		<View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>
                <Text style={[textStyle.fullName, {marginBottom:10}]}>Create a new Hub</Text>
			    <Text style={[textStyle.smallDarkGreen, {marginBottom:10}]}>Please select a location</Text>

      <View style={mapStyle.createHubMap}>
  	    <MapView
            style={mapStyle.map}
  	        region={this.state.region}
  	        onRegionChange={this.onRegionChange}
            onPress={ (event) => {
              this.setState({
                longitude: event.nativeEvent.coordinate.longitude,
                latitude: event.nativeEvent.coordinate.latitude,
              });
            }
          }>
          <MapView.Marker coordinate={{
                      longitude: this.state.longitude,
      					      latitude: this.state.latitude,}} pinColor="#0CFFA0"  title={this.props.navigation.state.params.newHub.hubName}>

                        </MapView.Marker>
  	    </MapView>
      </View>


                <View style={containerStyle.inlineContainer}>
					<Text style={buttonStyle.nonActiveButton} onPress={this.onHandleCancel.bind(this)}>Cancel</Text>
					<Text style={buttonStyle.activeButton} onPress={this.onHandleCreate.bind(this)}>Create</Text>
				</View>
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
