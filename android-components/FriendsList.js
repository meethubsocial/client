/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import buttonStyle from '../styles/button';
import imgStyle from '../styles/img';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity, Alert
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class FriendsList extends Component<{}> {
  constructor(props) {
		super(props);
		this.removeFriend = this.removeFriend.bind(this);
		this.sendMsg = this.sendMsg.bind(this);
		this.viewProfile = this.viewProfile.bind(this);
		this.state = { 
			friends: [
				/*{id: 1, name:'Rachel Green', username:'r@chel.green', status:'Active', pic:'http://pixel.nymag.com/imgs/daily/vulture/2014/12/17/17-rachel-green-jewish.w529.h352.jpg'},
				{id: 2, name:'Monica Geller', username:'m0n1ca.geller', status:'3 days ago', pic:'http://filmesegames.com.br/wp-content/uploads/2014/09/Monica-Geller-monica-geller-25963717-1035-1280.jpg'},
				{id: 3, name:'Ross Geller', username:'r0ss.geller', status:'2 hours ago', pic:'https://img.buzzfeed.com/buzzfeed-static/static/2014-06/18/10/campaign_images/webdr07/33-of-the-most-memorable-ross-geller-moments-on-f-2-27100-1403100262-2_dblbig.jpg'},
				{id: 4, name:'Phoebe Buffay', username:'ph0ebe.buff@y', status:'Active', pic:'https://media1.popsugar-assets.com/files/thumbor/g1jJskizyWCzTH17cXNj2BGx0Ns/fit-in/1024x1024/filters:format_auto-!!-:strip_icc-!!-/2017/07/25/859/n/1922398/98c795d259779df2c77e61.43422979_edit_img_cover_file_43785815_1500923262/i/Phoebe-Buffay-Quotes-From-Friends.jpg'},
				{id: 5, name:'Joey Tribbiani', username:'j0ey.tribbi@ni', status:'Active', pic:'https://kinoafisha.ua/upload/2014/02/news/n6b/15/40466/b_1393571705mett-leblan-rasskazal-o-svoei-licsnoi-jizni.jpg'},
				{id: 6, name:'Chandler Bing', username:'ch@ndler.b1ng', status:'Active', pic:'http://www.freepngimg.com/download/lion/3-2-lion-png.png'}*/
			],imgUriPlaceholder: 'http://i67.tinypic.com/2zf4mrb.jpg',
		};
		//get initial data for view Profile:
		fetch('http://myvmlab.senecacollege.ca:6094/viewFriends', {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				session: this.props.navigation.state.params.session,
			}),
		}).then((response) => response.json())
		.then((responseData) => {
			if (responseData.success != false) {
				//insert
				for (i = 0; i < responseData.result.fullList.length; i++) {
					var friend = {
						id: responseData.result.fullList[i].UserId,
						name: responseData.result.fullList[i].First_Name + " " + responseData.result.fullList[i].Last_Name,
						username: responseData.result.fullList[i].Username,
						status: "Active",
						pic: responseData.result.fullList[i].Picture,
					}
					var newArray = this.state.friends.slice();
					newArray.push(friend);
					this.setState({ friends: newArray });
				}
			} else {
				alert("Something went wrong");
			}
		}).catch(function(error) {
			alert('An unexpected error occurred. Please try again later.');
			throw error;
		}); 
		//to access session: {this.props.navigation.state.params.session}
    }
  sendMsg(user){
		var session = this.props.navigation.state.params.session;
		var username = user.username;
    	this.props.navigation.navigate('OneOnOneChat',{session, username});
	} 
    viewProfile(user){
		var session = this.props.navigation.state.params.session;
		var username = user.username;
    	this.props.navigation.navigate('FriendProfile',{session, username});
	}
	removeFriend(user){
		Alert.alert(
			"Remove a friend",
			"Are you sure, you want to end friendship with "+user.name+"?",
			[{text: 'Yes', onPress: () => {
					//fetch request to remove friend
					fetch('http://myvmlab.senecacollege.ca:6094/deleteFriend', {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							session: this.props.navigation.state.params.session,
							username: user.username
						}),
					}).then((response) => response.json())
						.then((responseData) => {
							if (responseData.success != false) {
								//update list of friends
								alert(user.name+" was removed from your friend list");
								var session = this.props.navigation.state.params.session;
								this.props.navigation.navigate('FriendsList', {session});
							} else {
								alert("Something went wrong");
							}
						}).catch(function (error) {
							alert('An unexpected error occurred. Please try again later.');
							throw error;
						}); 
					}},
				{text: 'Cancel', onPress: () => {}, style: 'cancel'},
			],
			{ cancelable: false }
	)}
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
			<View style={containerStyle.container}>
				<Menu {...this.props}/>
	      <View style={containerStyle.mainComponent}>  
				<Text style={[textStyle.fullName]}>List of My Friends</Text>
				{this.state.friends.length > 0 ?  
					this.state.friends.map(( friend ) => {
		    		return (
							<View key={friend.id} style={[containerStyle.nameContainer, {marginTop: 10,  justifyContent: 'center', alignItems: 'center'}]}>
								
								<View style={[containerStyle.inlineContainer, {margin:3}]}>
									<Image style={imgStyle.profilePic} source={{uri: friend.pic}}
									  onError={(error)=>{this.refs['img'].setNativeProps({src: [{uri: this.state.imgUriPlaceholder}]})}}/>
									<View style={containerStyle.columnContainer}>
										<View style={[containerStyle.container, {backgroundColor:'#8EE4AF', margin:5}]}>
											<Text style={textStyle.smallDarkBlue} onPress={()=>this.viewProfile(friend)}>{friend.name}</Text>
										</View>
										<View style={[containerStyle.container, {backgroundColor:'#8EE4AF', margin:5}]}>
											<Text style={textStyle.smallDarkBlue}  onPress={()=>this.viewProfile(friend)}>{friend.username}</Text>
										</View>
										{/*<View style={[containerStyle.container, {backgroundColor: '#8EE4AF', margin:5}]}>
											<Text style={[textStyle.smallDarkBlue]}  onPress={()=>this.viewProfile(friend)}>{friend.status}</Text>
										</View>*/}

										<View style={[containerStyle.inlineContainer,{justifyContent: 'center', alignItems: 'center'}]}>
											<TouchableOpacity onPress={()=>this.sendMsg(friend)}>
												<Image style={imgStyle.menuIcon} source={require('../img/message.png')}/>
											</TouchableOpacity>
											<TouchableOpacity onPress={()=>this.removeFriend(friend)}>
												<Image style={imgStyle.menuIcon} source={require('../img/x.png')} />
											</TouchableOpacity>
										</View>
									</View>
								</View>

							</View>
						)})
					:<Text>Your Friend List is empty</Text>
				}
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
