/*
Written by Svitlana Galianova (UI part) & Giorgi Osadze (fetch part)
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import imgStyle from '../styles/img';
import formStyle from '../styles/form';
import buttonStyle from '../styles/button';
import Menu from '../elements/Menu';
import Message from '../elements/Message';
import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity, FlatList
} from 'react-native';
import sio from 'socket.io-client'
import { StackNavigator } from 'react-navigation';
import { List, ListItem } from "react-native-elements";


export default class HubChat extends Component<{}> {
  constructor(props) {
	  super(props);
    this.state = { hubName: this.props.navigation.state.params.hubName, hubId: this.props.navigation.state.params.hubId, userCount: '5',
      myUsername:"", messages:[], isGroupChat: true, newMsg:"", chatInterval:""
    };

    fetch('http://myvmlab.senecacollege.ca:6094/viewProfile', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			session: this.props.navigation.state.params.session,
		}),
	}).then((response) => response.json())
	.then((responseData) => {
		if(responseData.success != false){

			this.setState({
        myUsername: responseData.query[0].Username,
        messages:[
          /*{id:"1", username:responseData.query[0].Username, msg:"Hi"},
          {id:"2", username:"Eric", msg:"Hello guys, How are you doing?"},
          {id:"3", username:"Ronen", msg:"Hey, are you ready for the midterm?"},
          {id:"4", username:responseData.query[0].Username, msg:"Do we have a PRJ midterm as well????"},
          {id:"5", username:"Giorgi", msg:"Yes... He said that a month ago"},
          {id:"6", username:responseData.query[0].Username, msg:"A month??? Why didn't I know about that?"},
          {id:"7", username:"Eric", msg:"You weren't in a class that day"},
          {id:"8", username:"Ronen", msg:"But I texted you on Discord"},
          {id:"9", username:responseData.query[0].Username, msg:"Oh... I must have missed it"},
          {id:"10", username:"Giorgi", msg:"It's not big deal, just finish your parts of a project and you are good. Also did you finish working on chat already? I need it for testing"},
          {id:"11", username:responseData.query[0].Username, msg:"Not yet, I am working on it right now"},
          {id:"12", username:"Giorgi", msg:"Ok, let me know when you are done"}*/
        ]
			});

		}else{
			alert("Something went wrong");
		}
	}).catch(function(error) {
		alert('An unexpected error occurred. Please try again later.');
		throw error;
	});

  var session = this.props.navigation.state.params.session;
  this.getMsgIntervalId = setInterval(()=>{
  fetch('http://myvmlab.senecacollege.ca:6094/ggetmessages', {
      method: 'POST',
      headers: { Accept: 'application/json', 'Content-Type': 'application/json'},
      body: JSON.stringify({
        hubId:this.state.hubId,
        session:session
      })
    }).then(res => res.json()).then((res) => {
      //alert(JSON.stringify(res.messages));
      this.setState({messages:res.messages})
    })
    .catch((error) => {
      //console.error(error);
      //alert("error");
      throw error;
    }); // fetch request end
},1000);
  //this.setState(chatInterval:getMsgIntervalId);
  } // constructor
  componentWillUnmount(){
      clearInterval(this.getMsgIntervalId)
  }
    listUsers(){
      var session = this.props.navigation.state.params.session;
      var hubId = this.state.hubId
      var hubName = this.state.hubName
      this.props.navigation.navigate('HubUserList',{session, hubId, hubName});
    }
    onHandleSend() {
      if(this.state.newMsg != ""){
        //var session = this.props.navigation.state.params.session;
        //var msgNew = {id:this.state.messages.length+1, username:this.state.myUsername, msg:this.state.newMsg}
        //var newArray = this.state.messages.slice();
        //newArray.push(msgNew);


        fetch('http://myvmlab.senecacollege.ca:6094/gsendsingle', {
            method: 'POST',
            headers: { Accept: 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify({
              hubId:this.state.hubId,
              username:this.state.myUsername,
              msg:this.state.newMsg,
            })
          }).then(res => res.json()).then( (res) => {
            console.log("getsingle")
            this.setState({newMsg: ""});
          }).catch((error) => {
            //console.error(error);
            //alert("error");
            throw error;
          }); // fetch request end




        //this.setState({ messages: newArray });
      }
        /*fetch('http://myvmlab.senecacollege.ca:6094/sendmessage', {
            method: 'POST',
            headers: { Accept: 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify({
              username:this.state.myUsername,
              message:this.state.newMsg,
              session:session
            })
          }).then(res => res.json()).then( (res) => { })
          .catch((error) => {
            //console.error(error);
            //alert("error");
            throw error;
          }); // fetch request end

        //alert(this.state.newMsg);
      }else{
        alert("Empty msg");
      }*/

    }
    render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		    <View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>
            <View style={containerStyle.hubTitle}>
              <Text style={textStyle.hubTitle}>{this.state.hubName}</Text>
              <TouchableOpacity onPress={this.listUsers.bind(this)}>
                <Text style={textStyle.smallDarkGreen}>View active users</Text>
              </TouchableOpacity>
            </View>
            <View style={[containerStyle.container,{marginBottom:15}]}>
            {/*List of messages from this.state*/

            }
              <FlatList
                style={containerStyle.chat}
                behavior="padding" keyboardShouldPersistTaps="handled"
                data={this.state.messages}
                renderItem={({ item }) => (
                  <Message id={item.id} username={item.username} msg={item.msg}
                  isMe={(this.state.myUsername==item.username)?true:false}
                  />
                )}
                keyExtractor={item => item.id}
              />
        </View>
        <View style={containerStyle.container}>
          <TextInput
              underlineColorAndroid={'transparent'}
              style={formStyle.msgInputField}
              onChangeText={(text) => this.setState({ newMsg: text })}
              value={this.state.newMsg}
              placeholder={"Type your message here..."}
              placeholderTextColor="#379683"
              onSubmitEditing={this.onHandleSend.bind(this)}/>
        </View>
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
