/*
Written by Svitlana Galianova
for PRJ666, 2018
*/
import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import buttonStyle from '../styles/button';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity, Alert
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class ListHubs extends Component<{}> {
  constructor(props) {
	  super(props);
	  this.deleteHub = this.deleteHub.bind(this);
		this.state  = {
	    hasHubs: false, hubs:[],
		};
		//to access session: {this.props.navigation.state.params.session}

		fetch('http://myvmlab.senecacollege.ca:6094/viewHubByUser', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			session: this.props.navigation.state.params.session,
		}),
		}).then((response) => response.json())
		.then((responseData) => {
			if(responseData.success != false){
				if(responseData.result == undefined || responseData.result.length < 1){
					this.setState({hasHubs:false});
				}else{
					//alert(JSON.stringify(responseData));
					this.setState({
						hasHubs: true, hubs: responseData.result
					});
				}
			}else{
				alert("Something went wrong");
			}
		}).catch(function(error) {
			alert('An unexpected error occurred. Please try again later.');
			//throw error;
		});
  }
  onHandleCreateHub(){
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('CreateHub',{session});
	}
  deleteHub(hub){
	Alert.alert(
		"Delete a hub",
		"Are you sure, you want to delete "+hub.HubName+"?",
		[{text: 'Yes', onPress: () => {
			//fetch request to delete hub
			fetch('http://myvmlab.senecacollege.ca:6094/deleteHub', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					session: this.props.navigation.state.params.session,
				}),
			}).then((response) => response.json())
				.then((responseData) => {
					if (responseData.success != false) {
						//update list of friends
						alert(hub.HubName+" was deleted");
						var session = this.props.navigation.state.params.session;
						this.props.navigation.navigate('ListHubs', {session});
					} else {
						alert("Something went wrong");
					}
				}).catch(function (error) {
					alert('An unexpected error occurred. Please try again later.');
					throw error;
				}); 
			}},
			{text: 'Cancel', onPress: () => {}, style: 'cancel'},
		],
		{ cancelable: false }
	)}
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		<View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>
				<Text style={[textStyle.fullName]}>List of My Hubs</Text>
				<TouchableOpacity onPress={this.onHandleCreateHub.bind(this)}>
            		<Text style={[buttonStyle.activeButton]}>Create New Hub</Text>
          		</TouchableOpacity>
				{this.state.hasHubs == true ? this.state.hubs.map(( hub ) => {
					return (
						<View key={hub.HubId} style={[containerStyle.nameContainer, {marginTop: 10,  justifyContent: 'center', alignItems: 'center'}]}>
							<View style={[containerStyle.container, {backgroundColor: '#8EE4AF', margin: 20}]}>
								<Text style={[textStyle.smallDarkBlue, {fontSize: 25}]}>{hub.HubName}</Text>
							</View>
							<TouchableOpacity onPress={()=>this.deleteHub(hub)}>
								<Text style={[buttonStyle.nonActiveButton]}>Delete This Hub</Text>
							</TouchableOpacity>
							<View style={[containerStyle.container, {marginBottom: 10}]}>
								<Text style={[textStyle.smallDarkBlue]}>Starts On: {hub.StartTime}</Text>
								<Text style={[textStyle.smallDarkBlue]}>Ends On: {hub.EndTime}</Text>
							</View>
						</View>
						)})
				:<Text>You do not have any upcoming hubs</Text>}
		  </View>
	  </View>
	</ScrollView>
	);
}
}
