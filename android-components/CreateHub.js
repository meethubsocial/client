/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import formStyle from '../styles/form';
import buttonStyle from '../styles/button';
import validation from '../elements/validationWrapper';
import random from '../elements/random';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, CheckBox, Keyboard,
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import customValidation from '../elements/validationWrapper';

export default class CreateHub extends Component<{}> {
  constructor(props) {
    super(props);
    var today = new Date();

    this.state = { newHub:{ hubName: '', range: '',startTime:'', endTime:'',},
      dd:'', mm:'', yyyy:'', startHH:'', startMin:'', endHH:'', endMin:'',
      timeError:'',hubNameError:'',rangeError:'',dateError:''};
      //to access session: {this.props.navigation.state.params.session}
    }
  onHandleEndTime(){
    this.setState({selected:'end'},()=>{
      random.timePicker.bind(this)
    })
  }
  onHandleCancel(){
    Keyboard.dismiss();
    var session = this.props.navigation.state.params.session;
    this.props.navigation.navigate('ListHubs',{session});
    //alert("Cancel");
  }
  onHandleNext(){
    Keyboard.dismiss();
    //trimming
    this.setState({newHub:{
      hubName: this.state.newHub.hubName.trim(),
      range: this.state.newHub.range.trim(),
      startTime:this.state.newHub.startTime,
      endTime:this.state.newHub.endTime,
    }},()=>{
      //other validation
      var eCount = 0;
      const rError = customValidation.isValidRange(this.state.newHub.range);
      (rError == "")? eCount : eCount++;
      const hError = customValidation.validate('isHubEmpty', this.state.newHub.hubName);
      (hError == undefined)? eCount : eCount++;
      const dErorr = customValidation.isDateEmpty(this.state.dd, this.state.mm, this.state.yyyy);
      (dErorr == "")? eCount : eCount++;
      this.setState({hubNameError: hError, rangeError: rError,dateError: dErorr});

      if(eCount<=0){
        //sending kind of valid data
        var session = this.props.navigation.state.params.session;
        this.setState({
          hubName: this.state.newHub.hubName, range: this.state.newHub.range,
          startTime:this.state.yyyy+"/"+this.state.mm+"/"+this.state.dd+" "+this.state.startHH+":"+this.state.startMin+":00",
          endTime:this.state.yyyy+"/"+this.state.mm+"/"+this.state.dd+" "+this.state.endHH+":"+this.state.endMin+":00",
        },()=>{
          var newHub = this.state.newHub;
          this.props.navigation.navigate('CreateHubLocation', {newHub, session});
        });
      }
    });
  }
  render() {
	  return (
	  <ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
		<View style={containerStyle.container}>
		    <Menu {...this.props}/>
	        <View style={containerStyle.mainComponent}>
            <Text style={[textStyle.fullName, {marginBottom:10}]}>Create a new Hub</Text>

            <Text style={textStyle.error}>{this.state.hubNameError}</Text>
			      <TextInput
		          underlineColorAndroid={'transparent'}
              style={formStyle.inputField}
              value={this.state.newHub.hubName}
              onChangeText={(text) => this.setState({ newHub:{
                hubName: text, range: this.state.newHub.range,
                startTime:this.state.newHub.startTime,
                endTime:this.state.newHub.endTime,}})}
		          placeholder={"Hub Name"}
		          placeholderTextColor="#379683" />

            <Text style={textStyle.error}>{this.state.rangeError}</Text>
            <TextInput keyboardType='numeric'
		          underlineColorAndroid={'transparent'}
              style={formStyle.inputField}
              value={this.state.newHub.range}
              onChangeText={(text) => this.setState({ newHub:{
                hubName: this.state.newHub.hubName, range: text,
                startTime:this.state.newHub.startTime,
                endTime:this.state.newHub.endTime,}})}
		            placeholder={"Range (in meters)"}
		            placeholderTextColor="#379683" />

              <Text style={textStyle.error}>{this.state.dateError}</Text>
              <Text style={[formStyle.inputField]} onPress={random.datePickerStart.bind(this)}>
                {this.state.dd==''? 'Date': this.state.dd+"/"+this.state.mm+"/"+this.state.yyyy}
              </Text>

              <Text style={textStyle.error}>{this.state.timeError}</Text>
              <View style={containerStyle.inlineContainer}>
                <Text style={[formStyle.text, {marginRight:20}]} onPress={random.timePickerStart.bind(this)}>
                    {this.state.startHH==''?'Start Time': this.state.startHH+":"+this.state.startMin}
                </Text>
              </View>

          <View style={containerStyle.inlineContainer}>
            <Text style={buttonStyle.nonActiveButton} onPress={this.onHandleCancel.bind(this)}>Cancel</Text>
            <Text style={buttonStyle.activeButton} onPress={this.onHandleNext.bind(this)}>Next</Text>
          </View>
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
