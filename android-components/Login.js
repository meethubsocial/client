/*
Written by Svitlana Galianova
for PRJ666, 2018
*/
import buttonStyle from '../styles/button';
import containerStyle from '../styles/container';
import formStyle from '../styles/form';
import imgStyle from '../styles/img';
import textStyle from '../styles/text';

import customValidation from '../elements/validationWrapper';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, Button, TouchableOpacity, ScrollView, Keyboard,
} from 'react-native';

import { StackNavigator } from 'react-navigation';


export default class Login extends Component<{}> {
  constructor(props) {
	  super(props);
		this.state = { username: '', usernameError: '', password: '', 
		passwordError: '', session: '', errored: false};
	}
  onHandleLogin(){
		Keyboard.dismiss();
		this.setState({username:this.state.username.trim()}, ()=>{
		var eCount = 0;
		const uError = customValidation.validate('email', this.state.username);
		(uError == undefined)? eCount : eCount++;
		const pError = customValidation.validate('isEmpty', this.state.password);
		(pError == undefined)? eCount : eCount++;
		this.setState({
			usernameError: uError, passwordError: pError, errored: eCount,
		},()=>{
				if(this.state.errored<=0){
					//send state to Node.js server side
					fetch('http://myvmlab.senecacollege.ca:6094/login', {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
						username: this.state.username,
						password: this.state.password,
						}),
					}).then((response) => response.json())
					.then((responseData) => {
						if(responseData.session != undefined){
							this.setState({session: responseData.session},()=>{
								var session = this.state.session;
								this.props.navigation.navigate('HubsMap', {session});
							});
						}else{
							alert("Invalid email or password");
						}
					}).catch(function(error) {
							alert('An unexpected error occurred. Please try again later.');
							// ADD THIS THROW error
							throw error;
					});
				}
			});
		});
	}
	onHandleNavigate(){
		Keyboard.dismiss();
		this.props.navigation.navigate('Register');
	}
	onHandlePasswordRecovery(){
		Keyboard.dismiss();
		this.props.navigation.navigate('PasswordRecovery');
	}
  render() {
	  return (
		<ScrollView style={containerStyle.init} keyboardShouldPersistTaps="handled">
	    <View style={containerStyle.container}>
	      <View style={containerStyle.topTitle}>
	        <Text style={textStyle.titleText}> MeetHub </Text>
	      </View>
	      <View style={[containerStyle.mainComponent, {marginTop: 10}]}>
	        <Image style={imgStyle.loginIcon} source={require('../img/hands.png')} />
					<Text style={textStyle.error}>{this.state.usernameError}</Text>
	        <TextInput keyboardType='email-address'
		       underlineColorAndroid={'transparent'}
           style={formStyle.inputField}
           value={this.state.username}
           onChangeText={(text) => this.setState({ username: text })}
		       placeholder={"Email"}
		       placeholderTextColor="#379683" />
					 <Text style={textStyle.error}>{this.state.passwordError}</Text>
		      <TextInput
		       secureTextEntry={true}
		       underlineColorAndroid={'transparent'}
		       style={formStyle.inputField}
		       onChangeText={(text) => this.setState({ password: text })}
		       placeholder={"************************"}
		       placeholderTextColor="#379683"
           value={this.state.password}/>
					 <TouchableOpacity onPress={this.onHandleLogin.bind(this)}>
            <Text style={buttonStyle.activeButton}>Login</Text>
          </TouchableOpacity>
		      <Text style={textStyle.forgotPassword} onPress={this.onHandlePasswordRecovery.bind(this)}> Forgot my password </Text>
		      <Text style={textStyle.createAccount} onPress={this.onHandleNavigate.bind(this)}>I don't have an account</Text>
		    </View>
	    </View>
			</ScrollView>
	  );
  }
}
