/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import buttonStyle from '../styles/button';
import containerStyle from '../styles/container';
import formStyle from '../styles/form';
import imgStyle from '../styles/img';
import textStyle from '../styles/text';

import customValidation from '../elements/validationWrapper'; 

import React, { Component } from 'react';
import {
	Platform, Text, View, Image, TextInput, Button, TouchableOpacity, CheckBox, Keyboard, Linking,
	ScrollView, 
} from 'react-native';


export default class Register extends Component<{}> {
  constructor(props) {
	  super(props);
		this.state = { username: '', usernameErrorMsg: '', 
			password: '', passwordErrorMsg: '', 
			confirmPassword: '', confirmPasswordErrorMsg: '',
			lastName: '', lastNameErrorMsg: '', 
			firstName: '', firstNameErrorMsg: '',
			email: '', emailErrorMsg: '',  
			dob: '', dobErrorMsg: '', 
			checked: false, checkedErrorMsg: '', errored: 0};    
	}
	onHandleCancel(){
		Keyboard.dismiss();
		this.props.navigation.navigate('Login');
	}
	checkConfirmPassword(){
		if(this.state.password!=this.state.confirmPassword){
			return false;
		}
		return true;
	}
	onHandleRegister(){
		Keyboard.dismiss();
		//remove extra whitespaces
		this.setState({ 
			username: this.state.username.trim(),
			firstName: this.state.firstName.trim(),
			lastName: this.state.lastName.trim(),
			email: this.state.email.trim(),
			dob: this.state.dob.trim()
		}, function(){
			var eCount = 0;
			const uError = customValidation.validate('username', this.state.username);
			(uError == undefined)? eCount : eCount++;
			const fnError = customValidation.isValidName(this.state.firstName);
			(fnError == "")? eCount : eCount++;
			const lnError = customValidation.isValidName(this.state.lastName);
			(lnError == "")? eCount : eCount++;
			const eError = customValidation.validate('email', this.state.email);
			(eError == undefined)? eCount : eCount++; 
			const dError =  customValidation.isValidDate(this.state.dob);
			(dError == "")? eCount : eCount++;

			const pError = customValidation.validate('password', this.state.password);
			(pError == undefined)? eCount : eCount++;
			const cpError = customValidation.isValidConfirmPassword(this.state.password, this.state.confirmPassword);
			(cpError == "")? eCount : eCount++;
			var chError="";
			if(this.state.checked == false){
				chError="Please accept Terms & Conditions";
				eCount++;
			}
			this.setState({ 
				emailErrorMsg: eError, passwordErrorMsg: pError, confirmPasswordErrorMsg: cpError,
				dobErrorMsg: dError, firstNameErrorMsg: fnError, lastNameErrorMsg: lnError, 
				usernameErrorMsg: uError,checkedErrorMsg: chError, errored: eCount,
			},()=>{
				if(this.state.errored<=0){
					fetch('http://myvmlab.senecacollege.ca:6094/register', {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
							},
						body: JSON.stringify({
							username: this.state.username,
							password: this.state.password,
							lname: this.state.lastName,
							fname: this.state.firstName,
							email: this.state.email,
							birth: this.state.dob,
							acceptedTermsOfUse: this.state.checked
						}),
					  }).then((response) => response.json())
						.then((responseData) => {
							if(responseData.success == true){
								this.setState({session: responseData.session});
								var session = this.state.session;
								this.props.navigation.navigate('HubsMap', {session});
							}else{
								alert("Something went wrong. Please try again later");
							}
						 }).catch(function(error) {
							alert('An unexpected error occurred. Please try again later.');
					   // ADD THIS THROW error
						throw error;
						});
				}
			});
		});
	}
  render() {
	  return (
		 <ScrollView style={containerStyle.init} keyboardShouldPersistTaps="handled">
	    <View style={containerStyle.container}>
	      <View style={[containerStyle.topTitle, containerStyle.inlineContainer]}>
				  <Image style={imgStyle.topIcon} source={require('../img/hands.png')} />
	        <Text style={textStyle.titleText}> MeetHub </Text>
	      </View>
				<View style={[containerStyle.mainComponent, {marginTop: 10}]}>
	  			<Text style={textStyle.error}>{this.state.firstNameErrorMsg}</Text>
				<TextInput
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.firstName}
						onChangeText={(text) => this.setState({ firstName: text })}
						placeholder={"First Name"} 
						placeholderTextColor="#379683" />
				<Text style={textStyle.error}>{this.state.lastNameErrorMsg}</Text>
				<TextInput
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.lastName}
						onChangeText={(text) => this.setState({ lastName: text })}
						placeholder={"Last Name"} 
						placeholderTextColor="#379683" />
				<Text style={textStyle.error}>{this.state.emailErrorMsg}</Text>
				<TextInput
						keyboardType='email-address'
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.email}
						onChangeText={(text) => this.setState({ email: text })}
						placeholder={"Email Address"} 
						placeholderTextColor="#379683" />
				<Text style={textStyle.error}>{this.state.usernameErrorMsg}</Text>
				<TextInput
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.username}
						onChangeText={(text) => this.setState({ username: text })}
						placeholder={"Username"} 
						placeholderTextColor="#379683" />
				<Text style={textStyle.createAccount}>Date of Birth</Text>
				<Text style={textStyle.error}>{this.state.dobErrorMsg}</Text>
		    	<TextInput
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.dob}
						onChangeText={(text) => this.setState({ dob: text })}
						placeholder={"DD/MM/YYYY"} 
						placeholderTextColor="#379683" />
				<Text style={textStyle.error}>{this.state.passwordErrorMsg}</Text>
				<TextInput
						secureTextEntry={true}
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.password}
						onChangeText={(text) => this.setState({ password: text })}
						placeholder={"Password"} 
						placeholderTextColor="#379683" />
				<Text style={textStyle.error}>{this.state.confirmPasswordErrorMsg}</Text>
				<TextInput
						secureTextEntry={true}
						underlineColorAndroid={'transparent'}
						style={formStyle.inputField}
						value={this.state.confirmPassword}
						onChangeText={(text) => this.setState({ confirmPassword: text })}
						placeholder={"Confirm Password"} 
						placeholderTextColor="#379683" /> 
				<Text style={textStyle.error}>{this.state.checkedErrorMsg}</Text>
				<View style={containerStyle.inlineContainer}>
					<CheckBox value={this.state.checked} 
					  onValueChange={() => this.setState({ checked: !this.state.checked })} />
    			<Text style={{marginTop: 5}}> I accept </Text>
				<Text style={{color: 'blue',textDecorationLine: "underline"}}
					onPress={() => {Linking.openURL('http://www.osadze.com:3000/terms.html')}}>Terms of Use and Conditions</Text>
				
  			</View>
				<View style={containerStyle.inlineContainer}>
					<Text style={buttonStyle.nonActiveButton} onPress={this.onHandleCancel.bind(this)}>Cancel</Text>
					<Text style={buttonStyle.activeButton} onPress={this.onHandleRegister.bind(this)}>Register</Text>
				</View> 
				</View>
			</View>
			</ScrollView>
	  );
  }
}
