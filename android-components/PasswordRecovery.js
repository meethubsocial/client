/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import buttonStyle from '../styles/button';
import containerStyle from '../styles/container';
import formStyle from '../styles/form';
import imgStyle from '../styles/img';
import textStyle from '../styles/text';

import customValidation from '../elements/validationWrapper'; 

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, /*KeyboardAvoidingView,*/ Button, ScrollView, TouchableOpacity
} from 'react-native';

import { StackNavigator } from 'react-navigation';

export default class Login extends Component<{}> {
  constructor(props) {
	  super(props);
		this.state = { email: '', emailError: '', errored: 0, overlay: false}; 
    }
  onHandleNext(){
		this.setState({email:this.state.email.trim()}, ()=>{
			var eCount = 0;
			const eError = customValidation.validate('email', this.state.email);
			(eError == undefined)? eCount : eCount++;
			this.setState({ 
				emailError: eError, errored: eCount,
			},()=>{
				if(this.state.errored<=0){
					//this.setState({overlay: true});
					fetch('http://myvmlab.senecacollege.ca:6094/reset', {
						method: 'POST',
						headers: {
							Accept: 'application/json',
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
						email: this.state.email,
						}),
					}).then((response) => response.json())
					.then((responseData) => {
						if(responseData.success != false){
							alert('An email with a temporary password has been sent to your email address');
						}else{
							alert("We were not able to send an email. Please try again later.");
						}
					}).catch(function(error) {
							alert('There has been a problem with your fetch operation: ' + error.message);
							// ADD THIS THROW error
							throw error;
						});
					}
				});
		});
	}  
  render() {
		setBackgroundStyle = function(overlay){
			if(overlay) return containerStyle.overlay
			return {flex: 1}
		}
		setBoxStyle = function(overlay){
			if(overlay) return {flex: 1}
			return {flex: 1}
		}
	  return (
		/*<View style={setBackgroundStyle(this.state.overlay)}>
		<View style={containerStyle.overlay}>
		<View style={{flex: 1}}>
		<View style={containerStyle.alertBox}>
		<Text style={textStyle.alertBoxNotification}>An email with a temporary password
			has been sent to your a***@gmail.com address 
		</Text>
		<Text style={[buttonStyle.nonActiveButton, {fontSize: 30, alignSelf: 'center', marginTop: 20}]}>
			OK
		</Text>
		</View>
		<View style={[this.state.overlay ? containerStyle.overlay : {flex: 1}]}>*/
		<ScrollView style={containerStyle.init} behavior="padding" keyboardShouldPersistTaps="handled">
	    <View style={[containerStyle.container, {flex: 1, flexDirection: 'column', justifyContent: 'space-between',}]}>
	      <View style={containerStyle.topTitle}>
	        <Text style={textStyle.titleText}> MeetHub </Text>
	      </View>
	      <View style={[containerStyle.mainComponent]}>
	        <Image style={[imgStyle.loginIcon, {marginTop: 30, marginBottom: 30}]} source={require('../img/hands.png')} />
          <Text style={textStyle.createAccount}>Password Recovery</Text>
					<Text style={textStyle.error}>{this.state.emailError}</Text>
	        <TextInput
		       underlineColorAndroid={'transparent'}
           style={[formStyle.inputField, {marginBottom: 100}]}
           value={this.state.username}
           onChangeText={(text) => this.setState({ email: text })}
		       placeholder={"Email"} 
		       placeholderTextColor="#379683" />
		      <TouchableOpacity>
						<Text style={[buttonStyle.activeButton, {alignSelf: 'flex-end'}]} 
						onPress={this.onHandleNext.bind(this)} >Next</Text>
          </TouchableOpacity>
		    </View>
	    </View>
	  </ScrollView>
	  );
  }
}
