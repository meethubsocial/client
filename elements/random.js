/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

'use strict';

import { TimePickerAndroid, DatePickerAndroid } from 'react-native';

const random = {
    async timePickerStart(){
        const TimePickerModule = require('NativeModules').TimePickerAndroid;
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
                hour: 14,
                minute: 0,
                is24Hour: false, // Will display '2 PM'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
                // Selected hour (0-23), minute (0-59)
                var min=(minute<10)?"0"+minute:minute;
                var h=(hour<10)?"0"+hour:hour;
                this.setState({ startHH:h, startMin:min });
            }
        } catch ({code, message}) {
            alert('Cannot open time picker');
        }
    },
    async timePickerEnd(){
        const TimePickerModule = require('NativeModules').TimePickerAndroid;
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
                hour: 14,
                minute: 0,
                is24Hour: false, // Will display '2 PM'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
                // Selected hour (0-23), minute (0-59)
                var min=(minute<10)?"0"+minute:minute;
                var h=(hour<10)?"0"+hour:hour;
                this.setState({ endHH:h, endMin:min})
            }
        } catch ({code, message}) {
            alert('Cannot open time picker');
        }
    },
    async datePickerStart(){
        try {
            var today = new Date();
            var afterTomorrow = today.setDate(today.getDate()+2);
            const {action, year, month, day} = await DatePickerAndroid.open({
              date: new Date(),
              minDate: new Date(),
              maxDate: afterTomorrow,
              mode: 'calendar'
            });
            if (action !== DatePickerAndroid.dismissedAction) {
              // Selected year, month (0-11), day
              month=month+1;
              var m=(month<10)?"0"+month:month;
              var d=(day<10)?"0"+day:day;
              this.setState({dd:d, mm:m, yyyy:year});
            }
          } catch ({code, message}) {
            alert('Cannot open date picker: '+message);
          }
    }
}

export default random;