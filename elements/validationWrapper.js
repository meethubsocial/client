/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import validation from 'validate.js'

const customValidation = {
    validate(fieldName, value) {
        var constraints = {
        isEmpty:{
            presence: true,
            length: {
                minimum: 1,
                tooShort: '^Password is required'
            }
        },
        isHubEmpty:{
            presence: true,
            length: {
                minimum: 1,
                tooShort: '^Hub Name cannot be empty'
            }
        },
        email: {
            presence: true,
            email: {message: '^Please enter a valid email address'},
        },
        password: {
            presence: true,
            length: {
                minimum: 8,
                maximum: 16,
                tooShort: '^Password is too short',
                tooLong: '^Password is too long'
            },
            format: {
                pattern: "[a-z0-9]+",
                flags: "i",
                message: "^Password can only contain letters and numbers"
            }
        },
        username: {
            presence: true,
            length: {
                minimum: 4,
                maximum: 16,
                tooShort: '^Username is too short',
                tooLong: '^Username is too long'
            },
            format: {
                pattern: "[a-z0-9]+",
                flags: "i",
                message: "^Username can only contain letters and numbers"
            }
        },
        confirmPassword: {
            presence: true,
            /*equality: {
                attribute: 'password',
                message: 'Passwords do not match'
            }*/
        },
        dob: {
            presence: {message: "^DOB is required"},
            datetime: {
                dateOnly: true,
            },
            format: {
                pattern: "^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$",
                message: "^Invalid DOB format, try: DD/MM/YYYY"
            }
        },
        name: {
            presence: true,
            format: {
                pattern: "^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$",
                message: "^Invalid Name format"
            }
        },
        };

        var formValues = {}
        formValues[fieldName] = value

        var formFields = {}
        formFields[fieldName] = constraints[fieldName]


        const result = validation(formValues, formFields)

        if (result) {
            return result[fieldName][0]
        }
        return null
    },
    isValidRange(range){
        var patt = new RegExp("^[0-9]+$"); 
        if(patt.test(range)!= true)
            return "Range must be number(without desimal points)";
        if(range<1||range>1000)
            return "Range must be minimum 1m and maximum 1000m(1km)";
        return "";
    },
    isValidHourRange(startH, startM, endH, endM){
        if(startH==""||startM==""||endH==""||endM=="")
            return "Time cannot be empty";

        var startTime=startH*60+startM;
        var endTime=endH*60+endM;
        var timeDiff=endTime-startTime;
        //alert("valid hour range: "+startTime+" - "+endTime+" = "+timeDiff);
        if(timeDiff<0)
            return "Hub cannot start later than it ends...";
        if(startTime==endTime)
            return "Hub should last at least 1 minute";
        if(timeDiff>12000)
            return "Hub cannot be longer than 3 hours";
        return "";
    },
    isDateEmpty(d,m,y){
        if(d==""||m==""||y=="")
            return "Date cannot be empty";
        return "";
    },
    isValidDate(dateString){
        //Check if not blanc
        if(dateString=="")
            return "DOB is required";
        // Check for the pattern
        var patt = new RegExp("^[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9][0-9][0-9]$"); 
        if(patt.test(dateString) != true)
            return "Invalid date format, try: DD/MM/YYYY"; 

        // Parse the date parts to integers
        var parts = dateString.split("/");
        var day = parseInt(parts[0], 10);
        var month = parseInt(parts[1], 10);
        var year = parseInt(parts[2], 10);

        //Get current date
        var today = new Date();
        var todayDay = today.getDate();
        var todayMonth = today.getMonth()+1; //January is 0!
        var todayYear = today.getFullYear();
        // Check the ranges of year
        if(year < 1900 || year > (todayYear-12))
            return "You are too young... Come back in a few years";
        //Check the ranges of the month
        if( month == 0 || month > 12)
            return "There are only 12 months in a year";
            
        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
        var leap = "no";
        // Adjust for leap years
        if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
            monthLength[1] = 29;

        // Check the range of the day
        if(day < 0 || day > monthLength[month - 1])
            return "That date doesn't exist";
        return "";
    },
    isValidName(name){
        if(name.length < 2)
           return "Name cannot be that short";

        var hasDot = false;
        var hasApost = false;
        var hasDash = false;
        for(var i=0; i<name.length;i++){
            var valid = false;
            //check if first char is a letter
            if (!((name.charCodeAt(i)>64 && name.charCodeAt(i)<91)||
            (name.charCodeAt(i)>96 && name.charCodeAt(i)<123))&&i==0){
                return "Name must start with a letter";
            }else{
                valid = true;
            }
            //check if the last char is a letter or dot
            if (!((name.charCodeAt(i)>64 && name.charCodeAt(i)<91)||
            (name.charCodeAt(i)>96 && name.charCodeAt(i)<123)||name.charCodeAt(i)==46)&&i==(name.length-1)){
                return "Name may end only with a letter/dot";
            }else{
                valid = true;
            }
            //check for dot
            if(name.charCodeAt(i) == 46){
                if(hasDot!=true){
                    hasDot = true;
                    valid = true;
                }else
                    return "Only one dot is allowed";
            }
            //check apostrophe
            if(name.charCodeAt(i) == 39){
                if(hasApost!=true){
                    hasApost = true;
                    valid = true;
                }else
                    return "Only one apostrophe is allowed";
            }
            //check dash
            if(name.charCodeAt(i) == 45){
                if(hasDash!=true){
                    hasDash = true;
                    valid = true;
                }else
                    return "Only one hyphen is allowed";
            }//check for multiple spaces in a row
            if((name.charCodeAt(i) == 32&&name.charCodeAt(i-1) == 32)||
                (name.charCodeAt(i) == 32&&name.charCodeAt(i+1) == 32)){
                return "Name cannot have extra spaces in a row";
            }//check if space
            else{
                valid = true;
            }
            if(valid == false)
                return "Name is invalid";
        }
        return "";
    },
    isValidImgUri(uri){
        if(uri != ""){
            var patt = new RegExp("(?:https|http|HTTPS|HTTP)?:\/\/.*\.(?:jpg|JPG)"); 
            if(patt.test(uri) != true)
                return "Invalid image link (should be .jpg)";
        }
        return "";
    },
    isValidBio(bio){
        if(bio=="")
            return "Bio should not be empty";
        if(bio.length>255)
            return "Bio is too long: "+bio.length+", max length is 255";
        return "";
    },
    isValidConfirmPassword(password, confirmPassword){
        if(confirmPassword=="")
            return "Confirm Password is required";
        else if(confirmPassword!=password)
            return "Passwords are not the same";
        return "";
    }

}

export default customValidation
