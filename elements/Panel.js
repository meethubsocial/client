//Credit: https://github.com/hiteshsahu
//https://github.com/hiteshsahu/ReactNative-Expandable-List-View/blob/master/src/components/Panel.js
/*
Customized by Svitlana Galianova
for PRJ666, 2018
*/

import React, { Component } from 'react';
import { Text,View,Image,TouchableHighlight,Animated } from 'react-native';
import PanelStyle from '../styles/PanelStyle';

export default class Panel extends Component<{}>{
    constructor(props){
        super(props);

        this.icons = {
            'up'    : require('../img/Arrowhead-01-128.png'),
            'down'  : require('../img/Arrowhead-Down-01-128.png')
        };

        this.state = {
            title       : props.title,
            expanded    : false,
            animation   : new Animated.Value()
        };
    }

    toggle(){
        let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
            finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

        this.setState({
            expanded : !this.state.expanded
        });

        this.state.animation.setValue(initialValue);
        Animated.spring(
            this.state.animation,
            {
                toValue: finalValue
            }
        ).start();
    }

    _setMaxHeight(event) {
       if (!this.state.maxHeight) {
         this.setState({
           maxHeight: event.nativeEvent.layout.height+(event.nativeEvent.layout.height*0.5),
         });
       }
     }

     _setMinHeight(event) {
       if (!this.state.minHeight) {
         this.setState({
           minHeight: event.nativeEvent.layout.height,
           animation: new Animated.Value(event.nativeEvent.layout.height),
         });
       }
     }

    render(){
        let icon = this.icons['down'];

        if(this.state.expanded){
            icon = this.icons['up'];
        }

        return (
            <Animated.View
                style={[PanelStyle.container,{height: this.state.animation}]}>
                
                <TouchableHighlight
                    style={PanelStyle.button}
                    onPress={this.toggle.bind(this)}
                    underlayColor="#f1f1f1">
                    <View style={PanelStyle.titleContainer} onLayout={this._setMinHeight.bind(this)}>
                        <Text style={PanelStyle.list_header}>{this.state.title}</Text>
                        <Image style={PanelStyle.buttonImage} source={icon}></Image>
                    </View>
                </TouchableHighlight>
                

                <View style={PanelStyle.body} onLayout={this._setMaxHeight.bind(this)}>
                    {this.props.children}
                </View>

            </Animated.View>
        );
    }
}
