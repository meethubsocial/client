/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import containerStyle from '../styles/container';
import textStyle from '../styles/text';
import imgStyle from '../styles/img';
import Menu from '../elements/Menu';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity
} from 'react-native';

export default class Message extends React.PureComponent {

    render() {
      this.state={ 
        backgroundColor: (this.props.isMe==true) ? "#8EE4AF" : "#EDF5E1",  
        username: (this.props.isMe==true) ? "" : this.props.username,
        alignItems: (this.props.isMe==true) ? "flex-end" : "flex-start",
      }
      return (
          <View style={{ margin: 5,
            justifyContent: this.state.alignItems, 
            alignItems: this.state.alignItems
          }}>
            <View style={{backgroundColor: this.state.backgroundColor,padding:2,paddingLeft:5, paddingRight:5,}}>
              <Text style={textStyle.msgDarkGreen}>{this.props.username}</Text> 
              <Text style={textStyle.msgDarkBlue}>{this.props.msg}</Text>
            </View>
          </View>
      );
    }

  }