/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import buttonStyle from '../styles/button';
import containerStyle from '../styles/container';
import imgStyle from '../styles/img';
import textStyle from '../styles/text';

import React, { Component } from 'react';
import {
  Platform, Text, View, Image, TextInput, ScrollView, TouchableOpacity
} from 'react-native';
import { StackNavigator } from 'react-navigation';

export default class Menu extends Component<{}> {
	constructor(props) {
	  super(props);
  	}
	viewMyFriends(){ 
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('FriendsList', {session});
	}
	viewMyProfile(){
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('MyProfile', {session});
	}
	viewMyMessages(){
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('Messages',{session});
	}
	hub(){
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('ListHubs',{session});
	}
	viewSettings(){
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('EditProfile',{session});
	} 
	mainPage(){
		var session = this.props.navigation.state.params.session;
		this.props.navigation.navigate('HubsMap', {session});
	} 
  	render() {
	  return (
      <View>
		<Image style={imgStyle.menuImg} source={require('../img/hands.png')} />
		<View style={containerStyle.topTitle}>
	        <Text style={textStyle.menuTitleText} onPress={this.mainPage.bind(this)}>MeetHub</Text>
	    </View>
		<View style={containerStyle.menuContainer}>
			<View style={[containerStyle.menuIconsContainer, containerStyle.inlineContainer]}>
				<TouchableOpacity onPress={this.viewMyProfile.bind(this)}>
					<Image style={imgStyle.topMenuIcon} source={require('../img/user.png')}/>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.viewMyMessages.bind(this)}>
					<Image style={imgStyle.topMenuIcon} source={require('../img/message.png')}/>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.viewMyFriends.bind(this)}>
					<Image style={imgStyle.topMenuIcon} source={require('../img/friends.png')}/>
				</TouchableOpacity>
				<TouchableOpacity onPress={this.hub.bind(this)}>
					<Image style={imgStyle.topMenuIcon} source={require('../img/hub.png')} />
				</TouchableOpacity>
				<TouchableOpacity onPress={this.viewSettings.bind(this)}>
					<Image style={imgStyle.topMenuIcon} source={require('../img/settings.png')}/>
	  			   </TouchableOpacity>
				{/*<TouchableOpacity onPress={this.viewHelp.bind(this)}>
					<Image style={imgStyle.topMenuIcon} source={require('../img/help.png')}/>
				</TouchableOpacity>*/}
			</View>
		</View>
    </View>
	);
  }
}
