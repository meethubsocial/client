/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import React, {StyleSheet} from 'react-native';

const textStyle = StyleSheet.create({
    titleText: {
        fontSize: 45, color: '#05386B', textAlign: 'center', textAlignVertical: 'center',
    },
    menuTitleText: {
        fontSize: 45, color: '#05386B', textAlign: 'center', textAlignVertical: 'center', marginTop: 7, 
        marginBottom: 7, marginLeft: 100,
    },
    forgotPassword: {
        fontSize: 16, textAlign: 'center', textAlignVertical: 'center', color: '#8EE4AF', marginBottom: 30
    },
    createAccount: {
        fontSize: 16, textAlign: 'center', textAlignVertical: 'center', color: '#379683', backgroundColor: '#EDF5E1',
        padding: 3, marginBottom: 15
    },
    alertBoxNotification: {
        fontSize: 30, textAlign: 'center', textAlignVertical: 'center', color: '#05386B',

    },
    error: { fontSize: 12, color: '#FF5050', },
    warning:{ fontSize: 14, color: '#F4A460', },
    fullName: {
        fontSize: 25, color: '#05386B', textAlign: 'center', textAlignVertical: 'center', fontWeight: 'bold',
    },
    smallDarkBlue: {
        fontSize: 20, color: '#05386B', textAlign: 'center', textAlignVertical: 'center',
    },
    smallDarkGreen: {
        fontSize: 20, color: '#379683', textAlign: 'center', textAlignVertical: 'center',
    },
    msgDarkGreen: {
        fontSize: 15, color:'#379683',
    },
    msgDarkBlue: {
        fontSize: 15, color: '#05386B',
    },
    extraSmallDarkBlue: { fontSize: 13, color: '#05386B'},
    hubTitle:{
        fontSize:25, color: '#379683',margin:10, fontWeight: 'bold',
    },

})

module.exports = textStyle;
