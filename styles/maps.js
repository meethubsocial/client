import React, {StyleSheet} from 'react-native';
import Dimensions from 'Dimensions'
var {width, height } = Dimensions.get('window');

const mapStyle = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
      },
      createHubMap: {
        height:250,
        width:width-80,
      },
      inHub: {
        height:height-240,
        width:width,
      }
})

module.exports = mapStyle;
