/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

'use strict';
import { StyleSheet ,Dimensions} from 'react-native';
const { width, height } = Dimensions.get('window');

var Style = StyleSheet.create({
  list_container: { flex: 1, flexDirection: 'column',
    borderRadius: 4, margin:3,padding:10, backgroundColor : '#EDF5E1',
    paddingTop : 30, borderWidth: 2, borderColor: '#5CDB95',
  },
  list_item: { fontSize: 15, textAlign: 'center', textAlignVertical: 'center'},
  list_header: { fontSize: 25,margin:10,flex: 1,fontWeight :'bold',color:'#379683',
    textAlign: 'center', textAlignVertical: 'center'},
  list_sub_header: {fontSize: 17,fontWeight :'bold',},

  container: { backgroundColor: '#EDF5E1',margin:10, borderWidth: 2, borderRadius: 4,
    borderColor: '#5CDB95', overflow:'hidden',width:(width-40),
},
titleContainer : {flexDirection: 'row'},
title: {flex: 1, padding: 10, color:'#379683',fontWeight:'bold',},
button:{ },
buttonImage:{width:30,height:25},
body:{padding:10,paddingTop:0,justifyContent: 'center', alignItems: 'center'}

});

export default Style;