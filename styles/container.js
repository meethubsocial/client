/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import React, {StyleSheet} from 'react-native';
import Dimensions from 'Dimensions';

var { width, height } = Dimensions.get('window');

const containerStyle = StyleSheet.create({

    init: { flex: 1, backgroundColor: '#ffffff',},
    initChat:{ flex: 1, backgroundColor:'#ffffff', height:height},
    container: { flex: 1, flexDirection: 'column', },
    topTitle: {
        flex: 0.15, backgroundColor: '#EDF5E1', borderWidth: 2, borderColor: '#5CDB95', justifyContent: 'center', 
        alignItems: 'center',
    },
    mainComponent: {
        /*flex: 0.85*/flex: 1, justifyContent: 'center', alignItems: 'center',
    },
    inlineContainer: {flexDirection:'row', flexWrap:'wrap',},
    columnContainer: {flexDirection:'column',flex:1},
    hubTitle:{
        width: width, backgroundColor: '#EDF5E1', justifyContent: 'center', 
        alignItems: 'center', flexDirection:'row', flexWrap:'wrap', borderWidth: 2, borderColor: '#5CDB95',
    },
    menuContainer: {
        justifyContent: 'flex-end', alignItems: 'flex-end', backgroundColor: '#EDF5E1',
        borderWidth: 2, borderColor: '#5CDB95', marginBottom: 10, marginTop: 4,
    },
    menuIconsContainer: {
        marginRight: 12, marginBottom: 2, marginTop: 2
    },
    overlay: {
        flex: 1, position: 'absolute', left: 0, top: 0, bottom: 0, right:0, opacity: 0.5, 
        backgroundColor: '#8EE4AF', width: width, height: height, zIndex: 1,
    },
    alertBox: {
        position: 'absolute', left: (width/15), top: (height/3), bottom: (height/6), 
        right: (width/15),
        backgroundColor: '#EDF5E1', /*width: (width/1.5), height: 40,*/ zIndex: 2,
        padding: 30, borderRadius: 20,
    },
    nameContainer: {
        justifyContent: 'flex-end', alignItems: 'flex-end', backgroundColor: '#EDF5E1',
        width: width, borderRadius: 20,
    },
    chat:{
        width: (width-10), height: (height/2)
    },
    msgList:{
        width: (width-10)
    },
    
})

module.exports = containerStyle;
