/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import React, {StyleSheet} from 'react-native';

const buttonStyle = StyleSheet.create({
    activeButton: {
        fontSize: 18, backgroundColor: '#8EE4AF', borderColor: '#379683', borderWidth: 2, color: '#379683', 
        paddingLeft: 20, paddingRight: 20, paddingBottom: 7, paddingTop: 7, borderRadius: 7, margin: 5
    },
    nonActiveButton: {
        fontSize: 18, backgroundColor: '#8EE4AF', color: '#379683', paddingLeft: 20, paddingRight: 20, 
        paddingBottom: 7, paddingTop: 7, borderRadius: 7, margin: 5
    },
})

module.exports = buttonStyle;