/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import React, {StyleSheet} from 'react-native';
import Dimensions from 'Dimensions';

var { width, height } = Dimensions.get('window');

const imgStyle = StyleSheet.create({
    loginIcon: {
        width: 150, height: 150, borderWidth: 2, borderColor: '#5CDB95', marginBottom: 15, 
    },
    topIcon: {
        width: 80, height: 80, borderWidth: 2, borderColor: '#5CDB95', backgroundColor: '#ffffff',
    },
    topMenuIcon: {
        width: (width/28*3), height: (width/28*3), margin: 2, marginLeft: 6,
    },
    msgIcon:{
        width: (width/28*3), height: (width/28*3), 
    },
    menuIcon: {
        width: 40, height: 40, margin: 2, marginLeft: 6,
    },
    menuImg: {
        position: 'absolute', top: 7, left: 15, width: (width/7*2), height: (width/7*2), zIndex: 1,
        borderWidth: 2, borderColor: '#5CDB95', backgroundColor: '#ffffff',
    },
    profilePic: {
        width: 150, height: 150, borderColor: '#379683', borderWidth: 2, margin: 5,
    },
    editImg:{
        width: (width-100)/2, height:(width-100)/2, borderColor: '#379683', borderWidth: 2, marginBottom: 5,
        marginRight:30,
    }
})

module.exports = imgStyle;