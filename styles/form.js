/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import React, {StyleSheet} from 'react-native';
import Dimensions from 'Dimensions';

var { width, height } = Dimensions.get('window');

const formStyle = StyleSheet.create({
    inputField: {
        height: 40, width: (width-150), borderColor: '#379683', backgroundColor: '#EDF5E1', borderWidth: 2, 
        fontSize: 18, textAlign: 'left', textAlignVertical: 'center', color: '#379683', borderRadius:7,
        marginBottom: 15, padding: 10,
    },
    msgInputField:{
        height: 40, width: width-6, borderColor: '#379683', backgroundColor: '#EDF5E1', borderWidth: 2, 
        fontSize: 18, textAlign: 'left', textAlignVertical: 'center', color: '#379683', borderRadius:7,
        marginBottom: 15, padding: 10,
    },
    inlineInputField:{
        height: 40, width: (width-100)/2, borderColor: '#379683', backgroundColor: '#EDF5E1', borderWidth: 2, 
        fontSize: 18, textAlign: 'left', textAlignVertical: 'center', color: '#379683', borderRadius:7,
        marginBottom: 15, padding: 10,
    },
    textArea: {
        width: width-80, borderColor: '#379683', backgroundColor: '#EDF5E1', borderWidth: 2, 
        fontSize: 15, textAlign: 'left', textAlignVertical: 'center', color: '#379683', borderRadius:7,
        marginBottom: 15, padding: 10,
    },
    checkboxContainer: {
        width: 250, borderColor: '#379683', backgroundColor: '#EDF5E1', borderWidth: 2, 
        borderRadius:7,marginBottom: 15, padding: 10,
    },
    text: {
        borderColor: '#379683', backgroundColor: '#EDF5E1', borderWidth: 2, width:115,
        fontSize: 18, textAlign: 'left', textAlignVertical: 'center', color: '#379683', borderRadius:7,
        marginBottom: 15, padding: 5, paddingRight: 10, paddingLeft:10,
    },
})

module.exports = formStyle;
