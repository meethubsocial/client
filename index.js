/*
Written by Svitlana Galianova
for PRJ666, 2018
*/

import { AppRegistry } from 'react-native';
import Login from './android-components/Login';
import Register from './android-components/Register';
import PasswordRecovery from './android-components/PasswordRecovery';
import HubsMap from './android-components/HubsMap';
import FriendsList from './android-components/FriendsList';
import Messages from './android-components/Messages';
import ListHubs from './android-components/ListHubs';
import EditProfile from './android-components/EditProfile';
import Help from './android-components/Help';
import MyProfile from './android-components/MyProfile';
import CreateHub from './android-components/CreateHub';
import CreateHubLocation from './android-components/CreateHubLocation';
import InHub from './android-components/InHub';
import HubChat from './android-components/HubChat';
import HubUserList from './android-components/HubUserList';
import OneOnOneChat from './android-components/OneOnOneChat';
import FriendProfile from './android-components/FriendProfile';

import React, { Component } from 'react';
import { StackNavigator } from "react-navigation";


const loginWithNavigation = props => {
    return <Login navigation={props.navigation} />;
    //return <FriendProfile navigation={props.navigation} />;
  };
  
const MeetHub = StackNavigator({
    Login: { screen: loginWithNavigation },
    Register: { screen: Register },
    PasswordRecovery: { screen: PasswordRecovery },
    HubsMap: { screen: HubsMap }, 
    FriendsList: { screen: FriendsList },
    Messages: { screen: Messages }, 
    ListHubs: { screen: ListHubs },
    EditProfile: { screen: EditProfile },
    Help: { screen: Help },
    MyProfile: { screen: MyProfile },
    CreateHub: { screen: CreateHub },
    CreateHubLocation: { screen: CreateHubLocation },
    InHub: { screen: InHub },
    HubChat: { screen: HubChat },
    HubUserList: { screen: HubUserList },
    OneOnOneChat: { screen: OneOnOneChat },
    FriendProfile: { screen: FriendProfile },
    }, {
        headerMode: 'none',
        mode: 'modal'
});


AppRegistry.registerComponent('MeetHub', () => MeetHub);
